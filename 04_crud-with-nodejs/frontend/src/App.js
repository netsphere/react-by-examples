// Appコンポーネント

import React from 'react';
import PropTypes from 'prop-types';
// <Switch> は react-router v4 で追加され, v6 でまた廃止。<Routes> に変更.
import { BrowserRouter, Route,
         Routes } from 'react-router-dom';

//import './App.css';
import ArticleList from './pages/articles/List';
import ArticleShow from './pages/articles/Show';
import ArticleEdit from './pages/articles/Edit';
import About from './pages/About';
import Home from './pages/Home';
import NavItem from './components/NavItemRoute';
import NotFound from './pages/NotFound';


function App() {
    // ルーティングは, <BrowserRouter> で囲む.
    // <Link> or <NavLink> も <Router> の中に入れなければならない.
    // exact を付けないと, 部分一致になる.
    //
    // Bootstrap5: ml-auto (left)  => ms-auto (start)
    //             mr-auto (right) => me-auto (end)
    // react-router は, 毎回、破壊的変更を繰り返す. アホか
    //  - <Switch> は react-router v6 で単に廃止, <Routes> に変更.
    //  - <Route> について, component= と render= は element= に変更.
    return (<BrowserRouter>
  <nav className="navbar navbar-expand-sm navbar-light bg-light">
    <span className="navbar-brand">Posts</span>
    <button className="navbar-toggler" type="button" data-bs-toggle="collapse"
            data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false"
            aria-label="Toggle navigation">
      <span className="navbar-toggler-icon"></span>
    </button>
    <div className="collapse navbar-collapse" id="navbarSupportedContent">
      <ul className="navbar-nav me-auto">
        <NavItem exact to="/">Home</NavItem>
        <NavItem exact to="/articles/">Articles</NavItem>
        <NavItem       to="/about">About</NavItem>
      </ul>
    </div>
  </nav>
  <Routes>
    <Route exact path="/" element={<Home />} />
    <Route exact path="/articles" element={<ArticleList />} />
    <Route       path="/articles/new" element={<ArticleEdit />} />
    <Route exact path="/articles/:id" element={<ArticleShow />} />
    <Route       path="/articles/:id/edit" element={<ArticleEdit />} />
    <Route       path="/about" element={<About />} />

    <Route       path="/*" element={<NotFound />} />
  </Routes>
</BrowserRouter> );
}

export default App;
