
import React, {useState, useEffect} from 'react';
// <Redirect> は react-router v6 で単に廃止。<Navigate> に置き換え.
import { Link, useParams, useNavigate } from 'react-router-dom';
import Article from '../../models/Article';


// 編集ページ
function ArticleEdit() // extends React.Component
{
    // react-router v6: URL 引数の受け取り方が変わった。ヒドい。
    const params = useParams();
    // react-router v6: リダイレクトのやり方も変わった。
    const navigate = useNavigate();

    let { id } = params;

    const [state, setState] = useState({
        isLoaded: Number(id) > 0 ? false : true,
        error: null,
        submitEnabled: false,

        // 'Post' fields
        id: null,
        title: '',
        body: ''  });

    useEffect( () => { // componentDidMount() {
        //let { id } = params;
        if (Number(id) > 0 && !state.isLoaded ) {
            // edit
            Article.find(id).then( post => {
                setState({...state,
                    isLoaded: true,
                    error:null,
                    id:    post.id,
                    title: post.title,
                    body:  post.body });
            });
        }
        /* else {
            // new
            this.setState({
                isLoaded: true,
                submitEnabled: false,   });
        } */
    });

    const handleSubmit = async (event) => {
        event.preventDefault();
        const title = state.title.trim();
        if (!title) {
            return setState( {...state,
                              error:{message:'Title field must have at least 1 character.'},
                              submitEnabled:false });
        }

        Article.create({ id:state.id, title:title, body:state.body})
            .then( article => {
                // Succeeded. Redirect
                navigate(`/articles/${article.id}`,
                         {state:{flash:`Item "${article.title}" updated.`}});
            }).catch( err => {
                err.response.then(res_body => {
                    setState({...state, error: res_body.error,
                              submitEnabled:false });
                });
            });
    }

    // リアルタイム更新
    const onFieldChanged = (event) => {
        const target = event.target;
        const name = target.name;
        // checkbox の場合分け必要. event.target.checked
        const value = target.type === 'checkbox' ? target.checked : target.value;
        setState( {...state,
                   submitEnabled: (name === 'title' ? value : state.title).trim() !== '',
                   [name]:value });
    };

    console.log(state.error);
    return (<>
  {state.error ? <div class="alert alert-danger" role="alert">
                        Error: {state.error.message}</div> : ''}
  { !state.isLoaded ? (
  <div>Loading...</div>
                ) : (
  <div>
    <form onSubmit={handleSubmit} >
      <div class="form-group row">
        <label className="col-sm-2 col-form-label">Title:</label>
        <div class="col-sm-10">
          <input name="title" type="text" class="form-control"
                 value={state.title}
                 onChange={e => onFieldChanged(e)} />
        </div>
      </div>
      <div class="form-group row">
        <label className="col-sm-2 col-form-label">Body:</label>
        <div class="col-sm-10">
          <textarea name="body" class="form-control" value={state.body}
                    onChange={e => onFieldChanged(e)} />
        </div>
      </div>
      <Link to={state.id ? `/articles/${state.id}` : `/articles/`}
            replace className="btn btn-secondary">Cancel</Link>
      <button type="submit" class="btn btn-primary"
              disabled={state.submitEnabled ? '' : 'disabled'}>
        {Number(state.id) > 0 ? 'Update' : 'Create'}</button>
    </form>
  </div> )}
            </> );
}


export default ArticleEdit;
