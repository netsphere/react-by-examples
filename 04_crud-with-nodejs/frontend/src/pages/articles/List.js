
import React, {useState, useEffect} from 'react';
// react-router v6: location の取り出し方が変わった
// React Hook "useLocation" cannot be called in a class component. React Hooks
// must be called in a React function component or a custom React Hook function.
import { Link, useLocation } from 'react-router-dom';
import Article from '../../models/Article';


function ArticleList() // extends React.Component
{
    // react-router v6: props.location が来てない。ヒドい。
    const location = useLocation();

    // Ajax 呼び出ししてよいタイミングは限定されている.
    // See https://reactjs.org/docs/faq-ajax.html
    const [state, setState] = useState({
        flash: location.state ? location.state.flash : null,
        error: null,
        isLoaded: false,
        posts: []   });

    useEffect( () => { // componentDidMount() {
        if (!state.isLoaded) {
            Article.find_all().then( posts => {
                setState({...state,
                          isLoaded: true,
                          error: null,
                          posts: posts   });
            }).catch( err => {
                setState({...state, error: err });
            });
        }
    } );

    return (<>
  <h1>Article List</h1>
  { state.error ? <div class="alert alert-danger" role="alert">
                    Error: {state.error.constructor.name} {state.error.message}
                  </div> : ''}
  { !state.isLoaded ? (
  <div>Loading...</div>
                ) : (
  <div>
  {state.flash ? <div class="alert alert-success">{state.flash}</div> : ''}

    <p><Link to="/articles/new">New article...</Link></p>
    <table class="table">
      <thead>
        <tr><th>#</th> <th>Title</th></tr>
      </thead>
      <tbody>
            {state.posts.map(post => (
        <tr>
          <td>{post.id}</td> <td>{post.title}</td>
          <td><Link to={`/articles/${post.id}`}>Show</Link></td>
        </tr>
            ))}
      </tbody>
    </table>
  </div> )}
            </> );
}


export default ArticleList;
