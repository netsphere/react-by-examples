
import React from 'react';
import {Link} from 'react-router-dom';

// SPA では HTTP レスポンスを返すわけではない。表示だけおこなう。
const NotFound = () => {
    // Response オブジェクトを返すのはエラー.
    return (<>
  <h1 style={{color:"red", fontSize:"40pt"}}>404 Not Found</h1>
  <p><Link to="/">Go Home</Link></p>
            </> );
}

export default NotFound;
