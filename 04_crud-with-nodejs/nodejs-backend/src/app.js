'use strict'; // Node.js は明示しないといけない.

// From https://expressjs.com/ja/starter/hello-world.html

const express = require('express');
// ExpressJS v4.16.0 で, body-parser相当の機能が組み込まれた。
//const bodyparser = require('body-parser');

const articles = require('./articles');

// Express application
const app = express();

// middleware
//app.use(bodyparser.bodyParser.json());
app.use(express.json());

// routing
app.use('/api/articles', articles);

// 起動
const PORT = process.env.PORT || 3001;

// backend は, localhost に限定しなければならない
app.listen(PORT, 'localhost', function() {
    console.log("Example app listening on port %d in '%s' mode",
                PORT, app.settings.env);
});
