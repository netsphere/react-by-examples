'use strict'; // Node.js は明示しないといけない.

// ルーティング (とハンドラ本体) を定義する
// このサンプルでは、全部のハンドラを1ファイルに収めているが, URLで分割もできる

const express = require('express');
const models = require('./models');

const router = express.Router();

const ResourceUrl = '/:id';

// List
router.get('/', async function(request, response) {
    const articles = await models.Article.findAll();
    response.json(articles);
});


// Show an item
router.get(ResourceUrl, async function(request, response) {
    var result;
    if (request.params.id == 'latest') {
        result = await models.Article.findAll({  // 配列で返す
            order: [['updatedAt', 'DESC']],
            limit: 1 });
    }
    else {
        result = await models.Article.findOne({
            where: {id:request.params.id}   });
        if (!result) {
            // send(body, status) is deprecated.
            return response.status(404).json({
                "error": {
                    "code": "RecordNotFound",  // string
                    "message": "Record not found",
                    "target": "query" } // 追加情報は "details": [{...}, ...]
            });

        }
    }
    response.json(result);
});


//////////////////////////////////////////
// Create new item.

router.article_create = async function(request, response) {
    var article = null;
    try {
        // create() = build() + save().
        article = await models.Article.create(request.body);
    }
    catch (err) {
/*
ValidationError [SequelizeValidationError]: Validation error: NG word!!
  errors: [
    ValidationErrorItem {
      message: 'NG word!!',
      type: 'Validation error',
      path: 'title',                <- ここでどのフィールドか区別できる
      value: 'NG',
      origin: 'FUNCTION',
      instance: [Article],
      validatorKey: 'myCustom',
      validatorName: null,
      validatorArgs: [],
      original: Error: NG word!!
*/
        console.log(err); // DEBUG
        return response.status(400).json({
                "error": {
                    "code": "RecordInvalid", // Bad Request
                    "message": err.message || "article_create() failed",
                    "details": err.errors }
        });
    }

    console.log(article.id);
    response.json(article);
};
router.post('/', router.article_create);


// Update a item.
router.article_update = async function(request, response) {
    var article = await models.Article.findOne({
                            where: {id:request.params.id}
    });
    if (!article) {
        return response.status(404).json({
            "error": {
                "code": "RecordNotFound",
                "message": "Record not found", }
        });
    }

    try {
        article.title = request.body.title;
        article.body = request.body.body;
        await article.save();
    }
    catch (err) {
        return response.status(400).json({
                "error": {
                    "code": "RecordInvalid", // Bad Request
                    "message": err.message || "article_update() failed",
                    "details": err.errors }
        });
    }

    response.json( article );
};
router.put(ResourceUrl, router.article_update);


// destroy
router.delete(ResourceUrl, async function(request, response) {
    const article = await models.Article.findOne({
                            where: {id:request.params.id}
    });
    if (!article) {
        return response.status(404).json({
            "error": {
                "code": "RecordNotFound",
                "message": 'Record not found', }
        });
    }

    try {
        await article.destroy();
    }
    catch (err) {
        return response.status(400).json({
                "error": {
                    "code": "RecordInvalid", // Bad Request
                    "message": err.message || "delete() failed" }
        });
    }

    response.json(true);
});

module.exports = router;
