
# Example for Express API server with Sequelize

## How to run

1. 依存パッケージのインストール

```shell
  $ npm install
```

2. データベース設定
  `config/database.js` ファイル

* [dev] データベースファイルを作る。プロジェクトディレクトリにて,
```shell
  $ npx sequelize-cli db:create
```
      => sqlite の場合は、これは不要。

3. マイグレーション実行

<pre>
$ <kbd>npx sequelize-cli db:migrate</kbd>
Sequelize CLI [Node: 18.15.0, CLI: 6.6.0, ORM: 6.31.0]

Loaded configuration file "<code>config/database.js</code>".
Using environment "<code>development</code>".
== 20200328094506-create-article: migrating =======
== 20200328094506-create-article: migrated (0.069s)
</pre>



4. 開始

```shell
  $ npm start
```


Webブラウザでアクセス
  http://localhost:3001/api/articles
  




## API の呼出し方と error response

RESTful API は「考え方」であって、標準的な呼出し方が決まっているわけではない。ドキュメント頼みになってしまう。
REST API の標準呼出し方法や RPC over HTTP は、歴史的にいろいろなものが提案され、支配的な標準はない。とはいえ, いくつか使えそうな候補がある。

今回のサンプルは、まだページネーションすら何もなく、単に HTTP request を投げている。レスポンスは OData を真似てみた。

エラーレスポンスは、各仕様で key name はいろいろだが、複数のエラー項目を配列で持つのは同じ。



1. OData JSON Format. 
https://docs.oasis-open.org/odata/odata-json-format/v4.01/odata-json-format-v4.01.html  OData v4.0 が ISO/IEC 20802. de jure 標準.

Salesforce, SAP 社, MS Dynamics 365 などが利用. ASP.NET Core でも提供できる。

クエリが標準化されている。`$skip`, `$top` でページネーションできる。

エラーのトップレベルは `error` で, ただ一つのオブジェクトを持つ。`details` に複数書ける。

```json
{
  "error": {
    "code": "err123",
    "message": "Unsupported functionality",
    "target": "query",
    "details": [
      {
        "code": "forty-two",
        "target": "$search",
        "message": "$search query option not supported" }
    ],
    "innererror": {
      "trace": [...],
      "context": {...} }
  }
}
```



2. GraphQL
クエリ言語。REST (の考え方) では, リソースごとに複数リクエストしなければならず、効率が悪い。一つのリクエストでまとめてデータを取得できる。

採用例: GitHub GraphQL API, Shopify GraphQL Admin API など.

<a href="https://ichyaku.com/graphql-api-design/">Shopifyに学ぶ！GraphQL API設計5つのポイント</a>




2. SOAP 1.2 <Fault> XMLタグ. de jure 標準.

XML-RPC の発展形. まだ現役。ただ、XML なので、JavaScript からは使いにくいか。



3. ▲ RFC 7807 <i>Problem Details for HTTP APIs</i> (March 2016)

これはダメ。クエリとセットで定まった仕様でないと意味がない。

トップレベルがなく、HTTP response code で見分ける. `message` ではなく `title` にメッセージを書く。

```
HTTP/1.1 400 Bad Request
Content-Type: application/problem+json
Content-Language: en
```

```json
{
  "type": "https://example.net/validation-error",
  "title": "Your request parameters didn't validate.",
  "invalid-params": [ {
                         "name": "age",
                         "reason": "must be a positive integer" },
                       {
                         "name": "color",
                         "reason": "must be 'green', 'red' or 'blue'"}
                     ]
}
```




4. JSON:API v1.1   ... SuiteCRM (オープンソースCRM), Drupal などが採用
https://jsonapi.org/format/

Pagination は, <code>/?page[offset]=0&amp;page[limit]=10</code> のようにする。

エラーは, HTTPステイタスコードと、内容で示す。"422 Unprocessable Entity" は RFC 4918 (WebDAV) で定められている。-- RFC 9110 で <i>HTTP Semantics</i> にも取り込まれた.

トップレベルが `errors` で配列を持つ。

```
HTTP/1.1 422 Unprocessable Entity
Content-Type: application/vnd.api+json
```

```json
{
  "errors": [
    {
      "status": "422",
      "source": { "pointer": "/data/attributes/firstName" },
      "title":  "Invalid Attribute",
      "detail": "First name must contain at least two characters."
    }
  ]
}
```






5. JSON-RPC 2.0   ... いくつか採用例あり。最近では Ethereum クライアントでも。現役
https://www.jsonrpc.org/ 仕様は昔からある
。

JSON-RPC に限らず, JSON / XML でリクエスト・レスポンスをやりとりするのは、効率が悪い。バイナリフォーマットでの RPC over HTTP として, gRPC がある。これも選択肢。




6. OpenAPI (Swagger)
https://swagger.io/specification/  最新版は v3.1.0

フォーマットは JSON or YAML. これを同じセマンティクスで動かすため、ほかの REST API 仕様とはメッセージの毛色がかなり違う。

ASP.NET Core でも提供できる。というか標準。ドキュメントの自動生成などが強み。

採用例: ZOZO (wear.jp), クラウドサイン, tapple,
    fabric.inc (headless ecommerce platform) ほか多数

Pagination は、自分でAPI を定義してやらないといけない. マジ?  -- OpenAPI はクエリ標準を定めるもの *ではない*。

定め方は、例えばこの記事: <a href="https://blog.nitzano.com/generic-swagger-pagination">How to add generic pagination to swagger</a>



