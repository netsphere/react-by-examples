'use strict';

// 次のコマンドで, 自動的に生成される
// id, createdAt, updatedAt フィールドが自動的に追加される.
// $ npx sequelize-cli model:generate --name Article --attributes title:string,body:string

module.exports = {
    up: (queryInterface, Sequelize) => {
        // テーブル名は複数形
        return queryInterface.createTable('Articles', {
            id: {
                autoIncrement: true,  // serial
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            title: {
                type: Sequelize.STRING,  // 長さを省略すると 255
                allowNull: false,
            },
            body: {
                type: Sequelize.TEXT,
                allowNull: false,
            },
            created_at: {  // モデル上は 'createdAt'
                allowNull: false,
                type: Sequelize.DATE
            },
            updated_at: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },
    
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('Articles');
    }
};
