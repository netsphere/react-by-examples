"use client";

// See https://zenn.dev/kazukix/articles/react-compiler
//     React Compiler を試す
// `useMemo` や `useCallback` を手書きしなくてよくなる。

import React, { useState } from 'react';
import UserSub from './user_sub';

export default function Counter1 () {
  // `setCount()` は非同期関数. 値の更新は関数呼び出し時点ではない。
  const [count, setCount] = useState(0);

  const increment = () => {
      // setCount(count + 1);  誤った実装
      // 値ではなく 1引数の関数を渡すと, その時点での値を参照できる。
      setCount((prevCount) => prevCount + 1);
  };
  const decrement = () => {
      // setCount(count - 1);  誤った実装
      setCount((prevCount) => prevCount - 1);
  };
  const increment2 = () => {
      increment();
      increment();
  };
  const decrement2 = () => {
      decrement();
      decrement();
  };

    // 1. props を再生成 => <UserSub /> が再レンダーされる
    //    react compiler を有効にすると、再レンダーされない
    // 2. onNameClick={...} で `count` を利用すると、きちんと再レンダーされる
  return <div>
    <span>count = {count}</span>
    <UserSub user={{name: "John"}}
             onNameClick={() => console.log("clicked " /*+ count*/)} />
    <div>
      <button onClick={decrement2}>2 減らす</button>
      <button onClick={decrement}>1 減らす</button>
      <button onClick={increment}>1 増やす</button>
      <button onClick={increment2}>2 増やす</button>
    </div>
  </div> ;
}
