"use client";

import React from 'react';
import PropTypes from 'prop-types';

export default function UserSub(props) {
    console.log("User component rendered");
    return <div onClick={props.onNameClick}>Hello {props.user.name}</div>;
}

UserSub.propTypes = {
    user: PropTypes.object,
    onNameClick: PropTypes.func.isRequired,
};
