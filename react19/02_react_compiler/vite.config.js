
import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';
import { TanStackRouterVite } from '@tanstack/router-plugin/vite';

const ReactCompilerConfig = {
};

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [
        TanStackRouterVite({}),
        react({
            babel: {
                plugins: [
                    ['babel-plugin-react-compiler', ReactCompilerConfig]
                ], },
        })
    ],
}) ;
