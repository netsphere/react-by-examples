
# React Compiler sample

React Compiler を導入することで, `useMemo()` や `useCallback()` を手書きしなくてよくなる。起動時にトランスパイルし, re-renders を抑制するようなコード (今まで手書きしていたもの) を自動で差し込む。

https://github.com/reactwg/react-compiler/discussions/categories/announcements


## How to run

```shell
  $ yarn run dev
```

`src/counter1.jsx` ファイルに `<UserSub />` コンポーネントを置いている。あえて、常に props の実引数を再生成している。

外側のコンポーネントの状態 state に依存しない場合、count up しても, `<UserSub />` の再レンダーは不要なはずが、今までは再レンダーされてしまう。React Compiler はそれを見破って、再レンダーされないようなコードを生成する。

コメントアウトを外して, `count` を実引数として渡すようにすると、きちんと再レンダーされる。



## 作成手順

プロジェクトの生成
```shell
  $ yarn create @tanstack/router
```

`vite.config.js` に追加する。
```javascript
const ReactCompilerConfig = {
};

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [
        TanStackRouterVite({}),
        react({
            babel: {
                plugins: [
                    ['babel-plugin-react-compiler', ReactCompilerConfig]
                ], },
        })
    ],
}) ;
```

ESLint の設定もおこなう。

ESLint のセットアップ
```shell
  $ npm init @eslint/config
```
  
ESLint の実行
```shell
  $ npx eslint
```



  
