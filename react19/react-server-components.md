
# React Server Components (RSC)

React Server Components (RSC) の導入によって、React での開発方法が大きく変わる。
React v18 では実験的機能、React v19 で安定版として有効化。

Web 上の記事は、Next.js と混同した, 的外れなものがあまりに多い。

<a href="https://blog.vte.cx/react-server-components%E3%81%AF%E5%86%8D%E3%81%B3isormorphic%E3%81%AE%E5%A4%A2%E3%82%92%E8%A6%8B%E3%82%8B%E3%81%8B-e5ee1590b425">React Server Componentsは再びIsormorphicの夢を見るか - vtecx</a>
ここの図が一番的確。

<img src="1_8i5uIm0Rlg4xKwrCxHQ0wQ.webp" width="480" />

位置づけとして, サーバで動いていた Backends For Frontends (BFF) を置き換えるものが RSC. サーバで動くがフロントエンドの一部。したがい、BFF でデータベースを操作していたなら、RSC でデータベースを操作するようにする。BFF をそのままにして RSC から `fetch()` しては意味がない。

Next.js を前提とした記事では、サーバコンポーネントと旧来のクライアントコンポーネントを関数のように相互に呼び出すのに騙されて、サーバ・クライアント間のネットワーク上をレンダリングされたデータが流れることがスルーされがち。魔法じゃないんだから。

RSC のメリットとしてバンドルサイズ縮小が挙げられるが、BFF は元からサーバで動いていたので、これは的外れ。
BFF での API 構築、クライアント側での `fetch()` と解釈が不要になるのがメリット。

秘密情報をネットワーク上に流さないように、RSC endpoint でセキュリティの考慮が必要。試してみないと感触が掴めない。



## 実装面

次の順でレンダリングされる。

1. Webブラウザがサーバにリクエストする。

2. Stage 0 としてサーバでレンダリングされる。React ツリーを構築する。ルート要素は server component になる。client components の部分は穴を空けておく。

3. クライアントにここまでのレンダリング結果を渡す。HTML ではなく、独自の JSONフォーマットでシリアライズされたデータが流れる。Server components から client components に渡される props も。

4. Stage 1 として, Web ブラウザ上で client components がレンダリングされる。

<img src="react-server-components.png" width="480" /> https://www.plasmic.app/blog/how-react-server-components-work

例えばこの記事: <a href="https://zenn.dev/yuu104/articles/react-server-component">React Server Componentsを理解したい</a>

 - 一つひとつのコンポーネントごとに通信が発生するわけでなく、HTTP リクエストとレスポンスは 1回だけ。
 - 先にサーバコンポーネントがレンダリングされる以上、クライアントコンポーネントからサーバコンポーネントを呼び出せない。
 - Client component にしたい場合, <code>"use client";</code> を明記する。There is no directive for Server Components.



## フレームワーク

RSC を動かすには、サーバ側にランタイムが必要。RSC が不要なら, React v18 までと同様。


### 手書き

React + Vite だけでは RSC が動かない。<i>TanStack Router</i> も同様に, フレームワークではないので、そのままでは RSC が動かない (実行時エラー)。

サーバランタイムを手書きする場合は, react-server-dom-webpack パッケージを使う。

記事がある;
 - <a href="https://zenn.dev/uhyo/books/rsc-without-nextjs/viewer/client-rendering">Next.jsが出てこないReact Server Componentsハンズオン</a>
 - <a href="https://danielnagy.me/posts/Post_usaivhdu3j5d">Experimenting with React Server Components and Vite</a>

リポジトリもちょいちょい見つかる; (試していない)
 - https://github.com/unstubbable/mfng/
 - https://github.com/bholmesdev/simple-rsc/ 手書きサンプル
 


### フレームワーク

RSC に対応したフレームワークは、2024年11月現在、まだ少ない。
 - Next.js App Router     ○ 大げさ。
 - Remix v3 = React Router v7   × 開発中 https://react-router-docs-ja.techtalk.jp/start/rendering
 - Gatsby - 静的サイト生成 Static Site Generation (SSG) に特化. v5 で Partial Hydration としてサポート。https://www.gatsbyjs.com/docs/how-to/performance/partial-hydration/
 - Waku (枠) - The minimal React framework.      ○ https://waku.gg/  11月現在, v0.21.5. 開発中。`examples/` 以下に多くの例がある。

Waku (枠) かな。


### Astro

<i>Astro</i> - 超高速フレームワーク。デフォルト = SSG. SSR もサポート。React フレームワークではなく, 逆に Astroインテグレーションにより, React, Vue, Svelte, solid-js を利用可能。

React Server Components に似たモデルとして, Astro Islands がある。
 - Astroコンポーネントはすべてサーバでレンダリングされる
 - UIフレームワークコンポーネント (React など) を子要素として持てる。Web ブラウザ上でレンダリングされる
 - Astroコンポーネントが生成した静的コンテンツを <code>.astro</code> コンポーネントの中で, UIフレームワークコンポーネントに子要素として渡せる。

一緒やな。

たとえば https://reffect.co.jp/html/astro が詳しい。<code>.astro</code> 内でデータベースを操作している。BFF として使える。


### 余談: ASP.NET Core Blazor - Razor components

名前が滅茶苦茶紛らわしい。Razor Pages とは別モノ。

ASP.NET Core, .NET 8 から, コンポーネントごとに `@rendermode` ディレクティブでレンダーモードを指定。<a href="https://zenn.dev/microsoft/articles/aspnetcore-blazor-dotnet8-overview">.NET 8 の ASP.NET Core Blazor 新機能オーバービュー</a>

 - ルート要素は Static SSR. 子要素の render mode を切り替えるのは OK.
 - Interactive Server (サーバでレンダリングするが SignalR でリアルタイム更新) と Interactive WebAssembly (.NET ランタイムを Webブラウザで動かす) がある.
 - Interactive な Razor component の子に static SSR コンポーネントを持たせるのはいけそう (試していない)。

Interactive Server は, サーバでコードを実行するがクライアントと常時接続してリアルタイムに DOM を更新する。常時接続が前提なのと同時接続数がボトルネックになる。RSC とは似ても似つかない。

Interactive WebAssembly は、RSC と似た形になる。C# で isomorphic web applications を実現する。


