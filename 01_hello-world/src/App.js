// App コンポーネント

// css ファイルの import で, <head> 要素内, <style> に展開される.
//import logo from './logo.svg';
//import './App.css';
import React from 'react';

// index.js から呼び出される.
// コンポーネントは, React.Component から派生させる.
class App extends React.Component
{
    constructor(props) {
        super(props);
        this.state = {
            value: 10, // カウンタの初期値
        };
    }

    // function() *ではない*. this の扱いが違う.
    onIncr = () => {
        // state の更新によって、DOM に反映させる
        this.setState( {value: this.state.value + 1} );
    }

    // function() *ではない*
    onDecr = () => {
        this.setState( {value: this.state.value - 1} );
    }

    render() {
        return (
    <div className="App">
      <b>カウンタ:</b> {this.state.value}
      <div>
        <button type="button" onClick={this.onIncr}>+</button>
        <button type="button" onClick={this.onDecr}>-</button>
      </div>
    </div> );
    }
}


export default App;
