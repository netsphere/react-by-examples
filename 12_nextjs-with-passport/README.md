
# Next.js w/ Passport.js Example

認証を付ける

ここからコピー
https://github.com/vercel/next.js/tree/canary/examples/with-passport/

 - JavaScript (not TypeScript)
 - Pages Router
 - Passport.js
 
## How to run

```shell
  $ npm i
  $ npm run dev
```


## パッケージ選定

Next.js だと通常は Auth.js (旧 NextAuth.js) だろう。
しかし, データベースアダプタを介して直接データベースに書き込みにいくスタイルで、非常に微妙。よくない。
Passport がまだしも.

Web 上には <i>next-connect</i> を使うサンプルも多いが, next-connect はサーバ側をわざわざ <i>Express.js</i> みたいにするもの。ヴァージョン間の非互換もあり、むしろ使わない方がいい。

セッション管理は, next-session が有力だったが、低落傾向. 2年間リリースされておらず, 廃れた.
後継は iron-session パッケージ. 依存する iron-webcrypto は @hapi/iron の後継。



## Pages Router (`pages/`)

Pages Router の場合, `pages/api/` ディレクトリに置くと、それらはサーバ側で実行される (API Routes)。Public API になる。他方, App Router の場合は, API Routes の代わりに, Server Components or Route Handlers が使える。





## モデル

このサンプルはデータベースに保存しない。`lib/user.js` ファイルが出発点









This example show how to use [Passport.js](http://www.passportjs.org) with Next.js. The example features cookie based authentication with username and password.

The example shows how to do a login, signup and logout; and to get the user info using a hook with [SWR](https://swr.vercel.app).

A database is not included. You can use any database you want and add it [in this file](lib/user.js).

The login cookie is httpOnly, meaning it can only be accessed by the API, and it's encrypted using [@hapi/iron](https://hapi.dev/family/iron) for more security.

## Deploy your own

Deploy the example using [Vercel](https://vercel.com?utm_source=github&utm_medium=readme&utm_campaign=next-example):

[![Deploy with Vercel](https://vercel.com/button)](https://vercel.com/new/clone?repository-url=https://github.com/vercel/next.js/tree/canary/examples/with-passport&project-name=with-passport&repository-name=with-passport)

## How to use

Execute [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app) with [npm](https://docs.npmjs.com/cli/init), [Yarn](https://yarnpkg.com/lang/en/docs/cli/create/), or [pnpm](https://pnpm.io) to bootstrap the example:

```bash
npx create-next-app --example with-passport with-passport-app
```

```bash
yarn create next-app --example with-passport with-passport-app
```

```bash
pnpm create next-app --example with-passport with-passport-app
```

Deploy it to the cloud with [Vercel](https://vercel.com/new?utm_source=github&utm_medium=readme&utm_campaign=next-example) ([Documentation](https://nextjs.org/docs/deployment)).
