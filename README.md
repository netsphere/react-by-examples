
# React / Next.js by Examples

解説はこちら; https://www.nslabs.jp/react-next-examples.rhtml




## 素の React

<i>Create React App</i> (CRA)
  react-scripts package : babel, eslint, jest, postcss, webpack, etc.
```shell
npx create-react-app my-app
cd my-app
npm start
```

In 2023, the <i>Create React App</i> tool was deprecated.
<i>Vite</i> build tool is better than <i>webpack</i>.


### `01_hello-world/`

React だけを使うサンプル。画面にカウンタを表示し, [+] ボタン, [-] ボタンで加減算できる。

データの永続化は行わないので、reload すると初期化される。



### `02_todo-without-redux/`

<i>Vite</i>, React 19.

ToDo List アプリ。リスト表示, ToDo の追加, 状態変更が可能。ToDoリストのうちどれを表示するかフィルタが可能。

まだ永続化していないので、reload すると初期化される。

Redux は使っていない。`src/pages` 以下に各ページ, `src/components` 以下に部品を置くのが基本。このサンプルではまだ複数ページになっていないので、`src/pages` はない。



### `03_router-routing/`

React 18, React Router 6.

メモアプリ。一覧表示, 1件の表示, 追加・編集・削除が可能。まだ永続化していない。`src/ArticleModel.js` の配列に保存。

Bootstrap v5 で表示を整え。

react-router-dom v5 がルーティングを担当。react-router-dom は v4 で大きな破壊的変更が行われており, Web 上などの v3 までの解説は役に立たない。
※ React Router v6 でもまた破壊的変更があり、一筋縄ではいかない。アホか。



### `03.1_tanstack_router_counter/`

Counter component sample.




### `03.5_tanstack-router-sample/`

Posts, update (not yet)

`src/routes/posts_/$$postId/edit.jsx`





## How to connect React frontend to a backend.


### `04_crud-with-nodejs/`

このサンプルから, frontend と backend に分けている。

Frontend は Webブラウザ上で動くので, `fetch()` で backend のAPIを叩く。

Backend は Express v4 (Node.js), Sequelize v6 promise-based Node.js ORM, SQLite3 の組み合わせ。Migration でデータベーススキーマを生成する。



### `05_pagination-and-db-migration/`

Frontend で react-paginate パッケージを利用。

react-hook-form v7 を利用。Form ライブラリは formik と react-hook-form が2強。react-hook-form がトップで、勢いも強い。レンダリングのスピードも React Hook Form が勝る。
<a href="https://npmtrends.com/formik-vs-formsy-react-vs-react-final-form-vs-react-hook-form">formik vs formsy-react vs react-final-form vs react-hook-form</a>


Backend は引き続き, Express v4, Sequelize v6, SQLite3 の組み合わせ。Sequelize v6 でのトランザクションのやり方を示す。ただし、相当ダサい。





### `06_koa-react-full-example/`

パスワードによるユーザ登録、ログイン、ログアウトのサンプル。

#### Backend - Koa2

Backend は, Express の後継と目された <i>Koa2</i> に切り替え。Koa2 は Koa v1 とまったく別物なのに注意。Web上などの Koa v1向けの記事は役に立たない。ヒドい。

Sequelize もツラいので, <s>Bookshelf.js + </s>Knex SQL query builder に切り替え。データベースは SQLite3.

[2023-04] Bookshelf.js は, 最終リリースから3年が経過しており、開発終了. -- knex とヴァージョン不整合。もう使えない。
```
peer knex@">=0.15.0 <0.22.0" from bookshelf@1.2.0
```
移行先は, <a href="https://vincit.github.io/objection.js/">Objection.js</a> が良さそう。

認証 authentication は <i>Passport</i> がメジャー。<i>Connect</i> middleware として動作する


#### Frontend

Frontend は<s>React Router v5 の <code>&lt;Route></code> を拡張して</s>, ログイン中でなければリダイレクトするようにした。この方法が抜けなくできてよい。

React Router v6 対応ずみ: ほぼ全部書き直し。ヒドい。

Flash message を表示させるようにしている。`withRouter()` で囲まなければならないのが地味に盲点。





## Next.js アプリケィションフレームワーク

<i>Next.js</i> は、サーバまで込みのアプリケィションフレームワーク。グルっと時代が回ってきて、Rails - ORM ぐらいの位置づけ。


### `11_nextjs-blog-bookshelf/`

Next.js を使った CRUD サンプル。バックエンドまで JavaScript で書く前提であれば、別途 Express または Koa2 を立てる必要もなく, ずいぶん良い。

ORM は <i>Objection.js</i> を組み合わせた。



### `12_nextjs-with-passport/`

Next.js 認証サンプル。
 - JavaScript (not TypeScript)
 - Pages Router
 - Passport.js




## React 19 topics

React Compiler

<a href="react19/react-server-components.md">React Server Components (RSC)</a>


