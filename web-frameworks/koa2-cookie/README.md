
Koa v2 のサンプル


```sh
$ mkdir APP_NAME
$ cd APP_NAME
$ npm init
(対話的に作成)
$ npm install koa
```



## 認証系

<a href="https://www.passportjs.org/">Passport.js</a> がメジャー。 <a href="https://www.npmjs.com/package/passport/">passport - npm</a>

koa-passport パッケージが Koa v2 をサポート。似た名前の passport-koa2 はダメ。

koa-passport は koa-route パッケージに依存している。しかし koa-router パッケージを使わなければならない [MUST].
こういう本筋でない紛らわしさがあるので full-stack のほうが好ましい。

