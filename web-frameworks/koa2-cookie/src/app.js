'use strict'

// See https://www.tutorialspoint.com/koajs/koajs_sessions.htm
//     -> そのままでは動かない

const Koa = require('koa');
/*
 - koa-session は, ストレージに保管するには, 次の3メソッドを提供しろ;
      > get(key, maxAge, {rolling, ctx})
      > set(key, sess, maxAge, {rolling, changed, ctx})
      > destroy(key, {ctx})
▲どうやっても、セッションIDの差し替えができない. koa-session は採用できない.
*/
// koa-generic-session は、セッションID のコントロールが可能。
const koa_session = require('koa-generic-session');

// ルーティングだけでも (意味なく) 多くのパッケージがあるが, koa-router でよい.
// 二番手は koa-mount だが, 調べる必要なし.
const KoaRouter = require('koa-router');
const SessionStore = require('./session_store');

const app = new Koa(); // For Koa v1, use `var app = koa();`

app.keys = ['SECRET SECRET!!']; // Cookie secret. TODO: ファイルから設定.
const CONFIG = {
    // session-id のみがクッキーに保存される.
    // store を省略すると, koa-generic-session 添付の MemoryStore が使われる.
    store: new SessionStore(),

    // Cookie options:
    cookie: {
        // max-age と expires の両方が存在しない場合, セッションクッキーになる
        // See RFC 6265
        maxAge: 30 * 60 * 1000, // 30 min
        // signed: !!app.keys   -- sign the cookie value.
        // expires: new Date(0) -- a Date for cookie expiration.
        // path: '/'            -- cookie path, '/' by default.
        // domain: undefined    -- cookie domain
        // secure: false        -- https only. 本番環境では true にすること。
        // httpOnly: true       -- クライアント側の JavaScript からアクセス不可.
        //                         必ず true (true by default).
        // overwrite: false     -- whether to overwrite previously set cookies of
        //                      -- the same name (false by default).
        // sameSite: false
    }
};
app.use(koa_session(CONFIG)); // Includes the session middleware.

function myview(str, ctx) {
    ctx.type = 'html';
    ctx.body = '';
    if (ctx.session.flash && ctx.session.flash !== '') {
        ctx.body += '<em>' + ctx.session.flash + '</em>';
        ctx.session.flash = '';
    }
    ctx.body += str;
    ctx.body += '<a href="/sign_in">sign_in</a> <a href="/sign_out">sign_out</a>';
    ctx.body += JSON.stringify(ctx.session);
}

const router = new KoaRouter();
router.get('/', async (ctx, next) => {
    let n = ctx.session.views || 0;
    ctx.session.views = ++n;

    if (n === 1)
        myview('Welcome here for the first time!', ctx);
    else
        myview(`You have visited this page ${n} times!`, ctx);
});

router.get('/sign_in', async (ctx, next) => {
    const cur = ctx.session;
    // セッションの作り直し。セッションid も替わる。
    await ctx.regenerateSession(); // このawait がないとエラーになる。
    // セッションidだけを差し替えるメソッドはない。コピーしてやる.
    for (let k in cur)
        ctx.session[k] = cur[k];
    await ctx.saveSession(); // regenerateSession() の後は, 明示的に必要
    ctx.session.flash = 'Login succeeded.';
    ctx.redirect('/');
});

router.get('/sign_out', async (ctx, next) => {
    // ctx.session = null;  // これはセッションの削除。session id も生成されない
    // ctx.session = {}  // これだとセッションID は変わらない。ダメ
    await ctx.regenerateSession(); // await 必須.
    ctx.session.flash = 'Logout succeeded.';
    ctx.redirect('/');
});

app.use(router.routes());
app.use(router.allowedMethods());

// 起動
const PORT = 3000; // TODO: 設定ファイル
app.listen(PORT);
console.log(`Listen ${PORT}`);
