'use strict'

// For DEBUG

class SessionStore
{
    static _memory = {}

    constructor() {
    }

    async get(sid) {
        if (sid === null || sid === undefined)
            throw new Error('Session ID missing');
        console.log('get(): ' + sid ); // DEBUG
        // => get(): koa:sess:lsNsMm_uJ_o-4u4EcWfow7wYkNVow9Zh

        return SessionStore._memory[sid];
    }

    async set(sid, sess, ttl) {
        if (sid === null || sid === undefined)
            throw new Error('Session ID missing');
        console.log('set(): ' + sid + ', ' + JSON.stringify(sess), ttl ); // DEBUG
        // => set(): {"cookie":{"maxAge":1800000,"httpOnly":true,"path":"/","overwrite":true,"signed":true},"views":2} 1800000

        SessionStore._memory[sid] = sess;
    }

    async destroy(sid) {
        if ( sid === null || sid === undefined)
            throw new Error('Session ID missing');
        console.log('destroy(): ' + sid);
        SessionStore._memory[sid] = null;
    }
}

module.exports = SessionStore;
