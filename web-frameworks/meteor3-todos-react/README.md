
# Meteor3 React Sample

## How to run

```shell
$ yarn
$ meteor run
```




```shell
$ meteor list
ecmascript              0.16.10  Compiler plugin that supports ES2015+ in all .js files
es5-shim                4.8.1  Shims and polyfills to improve ECMAScript 5 support
hot-module-replacement  0.5.4  Update code in development without reloading the page
meteor-base             1.5.2  Packages that every Meteor app needs
mobile-experience       1.1.2  Packages for a great mobile user experience
mongo                   2.1.0  Adaptor for using MongoDB and Minimongo over DDP
react-meteor-data       3.0.3  React hook for reactively tracking Meteor data
reactive-var            1.0.13  Reactive variable
shell-server            0.6.1  Server-side component of the `meteor shell` command.
standard-minifier-css   1.9.3  Standard css minifier used with Meteor apps by default.
standard-minifier-js    3.0.0  Standard javascript minifiers used with Meteor apps by default.
static-html             1.4.0  Define static page content in .html files
typescript              5.6.3  Compiler plugin that compiles TypeScript and ECMAScript in .ts and .tsx files
```



<kbd>ps ax</kbd>
```
 1232 pts/0    Sl+    0:16 /home/horikawa/.meteor/packages/meteor-tool/.3.1.2.h9ndkk7lrql++os.linux.x86_64+web.browser+web.browser.legacy+web.cordova/mt-os.linux.x86_64/dev_bundle/bin/node --max-old-space
 1260 pts/0    Sl+    0:01 /home/horikawa/.meteor/packages/meteor-tool/.3.1.2.h9ndkk7lrql++os.linux.x86_64+web.browser+web.browser.legacy+web.cordova/mt-os.linux.x86_64/dev_bundle/mongodb/bin/mongod --bin
 1349 pts/0    Sl+    0:02 /home/horikawa/.meteor/packages/meteor-tool/.3.1.2.h9ndkk7lrql++os.linux.x86_64+web.browser+web.browser.legacy+web.cordova/mt-os.linux.x86_64/dev_bundle/bin/node --require=/home
```


