
import React from 'react';
import { Hello } from './Hello.jsx';
//import { Info } from './Info.jsx';

// Add
import { useTracker, useSubscribe } from "meteor/react-meteor-data";
import { TasksCollection } from "/imports/api/tasks_collection";
import { Task } from "./task";


export const App = () => {
    const isLoading = useSubscribe("tasks");  
    const tasks = useTracker(() => TasksCollection.find({}).fetch());

    const handleToggleChecked = ({ _id, isChecked }) =>
	  Meteor.callAsync("tasks.toggleChecked", { _id, isChecked });
    
    if (isLoading() ) {
	return <div>Loading...</div>;
    }
    
    return ( <div>
      <h1>Welcome to Meteor!</h1>

<Hello />
		 
<ul>
    { tasks.map(task => (
	<Task key={task._id} task={task} onCheckboxClick={handleToggleChecked} />
    )) }
</ul>

	     </div> );
};
