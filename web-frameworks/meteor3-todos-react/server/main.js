
import { Meteor } from 'meteor/meteor';
// Add
import { TasksCollection } from '/imports/api/tasks_collection';


const insertTask = (taskText) =>
  TasksCollection.insertAsync({ text: taskText });


// Callback
Meteor.startup(async () => {
    // Add
  if ((await TasksCollection.find().countAsync()) === 0) {
    [
      "First Task",
      "Second Task",
      "Third Task",
      "Fourth Task",
      "Fifth Task",
      "Sixth Task",
      "Seventh Task",
    ].forEach(insertTask);
  }

    // Add
    Meteor.publish("tasks", () => {
	return TasksCollection.find();
    });
    
});

