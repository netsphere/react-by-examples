
Based on https://github.com/AndrejGajdos/auth-flow-spa-node-react/
MIT License
動くようにするのが一苦労。


## package.json の整備

```shell
  $ npm install koa passport passport-local koa-router koa-generic-session koa-redis
  $ npm install dotenv
  $ npm install bcrypt passport-facebook passport-local 
```

 - koa-session はセッションIDの差し替えができない。koa-generic-session を使え.

 - koa-redis は, redis パッケージではなく, ioredis, connect-redis に依存.

 - koa-cors は廃れた. 代替は @koa/cors.
 
 - koa-send を明示的に package.json に書かなくてよい。koa-static から依存。


  "type": "module" にすると, import 文が使えるが, require() が使えなくなる。うーむ。



## 実行

このサンプルは Redis を使っている。Fedora 34 は Redis 6.2.5 をパッケージ。

Redis は ACID特性のトランザクションをサポートしていない。デザイン上の割り切り。そのため、アプリケィションの永続データの保管先としては妥当ではない。

dot.env.development.sample を .env.development にコピーし、編集.

  $ redis-server
  $ npm run start

