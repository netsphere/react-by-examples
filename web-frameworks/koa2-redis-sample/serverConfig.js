'use strict'; // Node.js は明示しないといけない.

const url = process.env.NODE_ENV === 'production' ?
                'https://auth-flow-in-spa.herokuapp.com' :
                `http://localhost:${process.env.PORT || 1337}`;

module.exports.facebookAuth = {
  clientID: 'xxx-xxx-xxx', // process.env.FACEBOOK_CLIENT_ID,
  clientSecret: 'yyy-yyy-yyy', // process.env.FACEBOOK_CLIENT_SECRET,
  callbackURL: `${url}/auth/facebook/callback`,
};

// production 環境でのみ使われる. See ./server.js
module.exports.redisConfig = {
  host: process.env.REDIS_HOST,
  port: 14939,
  password: process.env.REDIS_PASSWORD,
};
