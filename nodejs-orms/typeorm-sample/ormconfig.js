"use strict";

// 環境変数 NODE_ENV で選択する

// Settings by `.env.development` file
const ENV_NAME = process.env.NODE_ENV || "development";
const dotenv_result = require('dotenv')
             .config( {path:`.env.${ENV_NAME}`} );
if (dotenv_result.error) {
    throw dotenv_result.error;
}

// 環境ごとの設定
const env_config = {
    "development": {
        "type": "sqlite",
        "database": "database.sqlite"
    },
    "production": {
        "secret": process.env.DB_SECRET
    }
}

const base_config = {
    "synchronize": false,
    "logging": false,
    "entities": [
        "build/entity/**/*.js",
        "src/entity/**/*.ts" ],
    "migrations": [
        "src/migration/**/*.js"
    ],
    "subscribers": [
        "src/subscriber/**/*.ts"
    ],
    "cli": {
        "entitiesDir": "src/entity",
        "migrationsDir": "src/migration",
        "subscribersDir": "src/subscriber"
    }
}

module.exports = {
    ...base_config, ...env_config[ENV_NAME]
}
