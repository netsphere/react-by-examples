"use strict";

require("reflect-metadata");
const typeorm = require("typeorm"); // import {createConnection} from "typeorm";
const m = require("./entity/User"); // import {User} from "./entity/User";

const DbConfig = require(__dirname + '/../ormconfig.js');

async function test() {
    await typeorm.createConnection(DbConfig);

    console.log("Inserting a new user into the database...");
    let user = new m.User();
    user.firstName = "Timber";
    user.lastName = "田中";
    user.age = 35;
    await user.save();

    // Validation に失敗させる
    user = new m.User();
    user.firstName = "旅人";
    user.lastName = "星野";
    user.age = 5// 25;
    try {
        await user.save();
    }
    catch (err) {
        console.log(`Validation error: ''${err}''`);
    }

    console.log("Loading users from the database...");
    // ActiveRecord パタン
    const users = await m.User.find();
    console.log("Loaded users: ", users);
}

test();
console.log("Here you can setup and run express/koa/any other framework.");
