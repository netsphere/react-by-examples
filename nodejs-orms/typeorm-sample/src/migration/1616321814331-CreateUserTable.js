"use strict";

const typeorm = require("typeorm");
//import {MigrationInterface, QueryRunner} from "typeorm";

class CreateUserTable1616321814331 //extends typeorm.MigrationInterface
{
    async up(queryRunner) {
        await queryRunner.query(`CREATE TABLE users (id INTEGER PRIMARY KEY AUTOINCREMENT, first_name VARCHAR(200) NOT NULL, last_name VARCHAR(200) NOT NULL, age INT NOT NULL)`);
    }

    async down(queryRunner) {
        await queryRunner.query(`DROP TABLE users`);
    }
}

module.exports = CreateUserTable1616321814331;
