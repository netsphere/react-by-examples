// -*- mode:javascript -*-

// ActiveRecord パタンで作ってみる

import {BaseEntity, Entity, EntitySchema, PrimaryGeneratedColumn, Column, BeforeInsert, BeforeUpdate} from "typeorm";
import {Min, IsNotEmpty, validate} from "class-validator";

// ActiveRecord パタンでは, `BaseEntity` クラスから派生させる。
@Entity('users')
export class User extends BaseEntity
{
    @PrimaryGeneratedColumn()
    id: number;

    @Column({name:'first_name'})
    @IsNotEmpty()
    firstName: string;

    @Column({name:'last_name'})
    @IsNotEmpty()
    lastName: string;

    @Column()
    @Min(13)
    age: number;

    @BeforeInsert()
    @BeforeUpdate()
    async do_validate(): Promise<void> {
        const errors = await validate(this); // ValidationError[] 型
        if (errors && errors.length > 0) {
            throw new Error(JSON.stringify(errors));
        }
    }
}

/*
Error: [
  {"target":{"firstName":"Timber","lastName":"","age":5},
   "value":"",
   "property":"lastName",
   "children":[],
   "constraints":{"isNotEmpty":"lastName should not be empty"} },
  {"target":{"firstName":"Timber","lastName":"","age":5},
   "value":5,
   "property":"age",
   "children":[],
   "constraints":{"min":"age must not be less than 13"} }
]
 */
