
# TypeORM を調べる

TypeScript 必須

typeorm@0.3.20 -> ts-node@"^10.7.0"

● TypeORM 0.3 で指定方法が変わったらしい。ここを読んで、修正すること。
https://qiita.com/Aurum64/items/f5962bd2a643447dbef9

標準化された decorator への移行は困難。
issue: <a href="https://github.com/typeorm/typeorm/issues/9862">TypeScript 5.0 decorators support</a>




## Build with TypeORM

Steps to run this project:

1. Run `npm install` command

2. Setup database settings inside `ormconfig.js` and `.env.development` file

開発時: マイグレーションファイルの作成は、次のようにしないと上手くいかない.
<pre>
$ <kbd>npm run typeorm migration:create -- -n CreateUserTable</kbd>
</pre>

マイグレーション実行
<pre>
$ <kbd>npm run typeorm migration:run</kbd>
</pre>

3. Run `npm run dev` command



## プロジェクトの作成

プロジェクトの外側で <kbd>npx typeorm init</kbd> を叩くと, テンプレートが生成される.
SQLite3 のときは、末尾の 3 を付けない。(付けてもエラーは出ないが、出力されるテンプレートが不正.)

<pre>
$ <kbd>npx typeorm init --name MyFirstOrm --database sqlite</kbd>
npx: 72個のパッケージを18.185秒でインストールしました。
Project created inside /home/hori/repos/priv_react-by-examples/04_crud-with-nodejs/MyFirstOrm directory.
</pre>

次のファイルが生成される;

<pre>
ormconfig.json
README.md
src/
  +- index.ts
  +- migration/
  +- entity/
       +- User.ts
.gitignore
package.json
tsconfig.json
</pre>

<code>src/</code> の下に <code>migration</code>, <code>entity</code> ディレクトリができる。いずれも単数形。




● <code>package.json</code> ファイルを修正. 全体的にバージョンが古い。

 ~   ~1.1.2 = 1.1.2 <= version < 1.2.0
 ^   ^1.2.3 := 1.2.3 <= version < 2.0.0
     ^0.2.3 := 0.2.3 <= version < 0.3.0  一番左側の 0 でない数字を固定
 >=  単純に, それ以上

<code>"devDependencies"</code>:
<dl>
  <dt>ts-node
  <dd>TypeScript execution and REPL for node.js, with source map support.
  Works with typescript@>=2.7.

  TypeScript のコードを直接実行. ない場合は, <kbd>npx tsc</kbd> でコンパイル要。
  
  <dt>@types/node
  <dd>This package contains type definitions for Node.js.

  <dt>typescript
  
</dl>

<code>"dependencies"</code>:

TypeScript は実行時は必要になっていない。

<dl>
  <dt>typeorm

  <dt>reflect-metadata
  <dd>必須。
  
  <dt>sqlite3
  <dd>こちらは 3 が付く.
</dl>



● <code>tsconfig.json</code> ファイルも見ておく

<code>"compilerOptions"</code>:
<dl>
  <dt><code>"target"</code>, <code>"lib"</code>
  <dd>生成されるのは <code>"es5"</code> で古い。Node.js v14 [2020年4月] は ES2020 の構文とライブラリの両方をほぼフルサポートしている。Node.js v12 は ES2019 を100%サポートしている。<code>"es2019"</code> でよか。

    <code>package.json</code> ファイルの <code>"engines"</code> で, Node.js のバージョンを指定する。
  
  <dt>"module": "commonjs", "moduleResolution": "node",
  <dd>これも "commonjs" (<code>target</code> が <code>es3</code> または <code>es5</code> のデフォルト.) で古いが, Node.js で動かすときはママ。

  <dt>"emitDecoratorMetadata": true, "experimentalDecorators": true,
  <dd>TypeORM は decorator 機能を使うため, 両方とも, <code>true</code> が必須。
  
  <dt>"strictPropertyInitialization": false
  <dd>これを書かないと, <kbd>npx tsc</kbd> で <code>src/entity/User.ts:7:5 - error TS2564: Property 'id' has no initializer and is not definitely assigned in the constructor.</code> のようなエラーが出る。

</dl>


● <code>ormconfig.js</code> ファイル

TypeORM の設定ファイルは, <code>ormconfig.json</code> か <code>.js</code> ファイルに書く。<i>dotenv</i> での振り分けなどから, <code>ormconfig.js</code> でいろいろするのが吉。

<dl>
  <dt>"synchronize": false,
  <dd>注意! これが <code>true</code> だと, モデル定義の変更で即時に database に反映. 必ず false にすること。
</dl>

