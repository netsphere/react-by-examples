
# Bookshelf.js サンプル

[2023-04] Bookshelf.js は, 最終リリースから3年が経過しており、開発終了か? -- knex とヴァージョン不整合。もう使えない。
  peer knex@">=0.15.0 <0.22.0" from bookshelf@1.2.0





Bookshelf.js は, Node.js で使われることを想定した, JavaScript ORM の一つ。<i>Knex</i> SQL クエリビルダを使っている。

https://bookshelfjs.org/

バージョン 0.15 系列と v1.0 以降とで、一部互換性がない。Web上の解説で、古いものが混ざっていることがよくある。





開発時: マイグレーションファイルの作成
$ npx knex migrate:make make_category

実行
$ npx knex migrate:latest

巻き戻す
$ npx knex migrate:rollback




## 実行

`knexfile.js.sample` を `knexfile.js` にコピーし、適宜編集。設定ファイルはこの名前でなければならない。

```
  $ npx knex migrate:latest
  $ npm run start
```

 
