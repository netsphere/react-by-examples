'use strict';

const db = require('./models');
const bookshelf = require('./connection');
// db.Article でもよい.
const Article = bookshelf.model('Article');
const Comment = bookshelf.model('Comment');

async function test() {
    // forge() は, 引数の値でモデルを初期化する。
    let post = Article.forge({
        title: 'Hogehoge',
        body:  'あいうえお♥㎞'
    });
    try {
        await post.save();
    }
    catch (err) {  // Validation に失敗した時は例外発生.
        console.log(err);
        return;
    }
    console.assert(post.id != 0);

    let comment = Comment.forge({
        article_id: post.id,
        comment: 'Hoge fuga'
    });
    await comment.save();

    // 読み直す.
    // 素の Bookshelf.js には findAll() や findOne() のようなメソッドはない.
    //    => `bookshelf-modelbase` plugin
    // withRelated: で子テーブルを同時に読み込める。
    let posts = await Article.where({}).orderBy('id')
                        .fetchAll(/*{withRelated: ['comments']}*/); // ここは複数形

    // withRelated: を付けたときは, comments も表示される. Eager loading.
    const article = posts.last();
    console.log(article.toJSON());
    //console.log( article );
    //console.log( article.related('comments').first() );

    // withRelated: を付けたときは, 単に related(...) で表示される.
    // そうでないときは, related() でも自動では読み込まれない. マジか!
    // => load() の明記が必要。
    await article.load('comments'); // 文字列で指定.
    const comments = await article.related('comments');
    console.log( comments.toJSON() );
}

test();
//dbconn.destroy();
