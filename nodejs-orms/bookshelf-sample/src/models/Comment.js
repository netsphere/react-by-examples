'use strict';

const bookshelf = require('../connection');

const Comment = bookshelf.Model.extend({
    tableName: 'comments',
    hasTimestamps: true,

    article: function() {
        return this.belongsTo('Article', 'article_id');
    }
});

module.exports = bookshelf.model('Comment', Comment);
