'use strict';

const bookshelf = require('../connection');
const Comment = require('./Comment');

const Article = bookshelf.Model.extend({
    tableName: 'articles',
    hasTimestamps: true,

    // これは relationship の定義であって, レコードの comments() を呼び出しても
    // 参照先の値は取れない.
    // ものすごい紛らわしい.
    comments: function() {
        return this.hasMany('Comment'); //, 'article_id');
    }
});

module.exports = bookshelf.model('Article', Article);
