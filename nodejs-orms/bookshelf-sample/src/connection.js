'use strict';

// 設定ファイル名は `/knexfile.js` でなければならない.
const dbconn = require('knex')(
    require(__dirname + '/../knexfile')[process.env.NODE_ENV || 'development'])
const bookshelf = require('bookshelf')(dbconn);

module.exports = bookshelf;
