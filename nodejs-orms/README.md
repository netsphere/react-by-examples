
# ORM を比べる

<i>Sequelize</i> を使っていると、いくつか不満がある。

 - 各テーブルのスキーマ定義が冗長。コンパイル時に型検査されるなら、スキーマ定義を明記する意味がある。しかし、JavaScript のような動的プログラミング言語では実行時にしかエラー検出しようがないし, SQL がエラーにしてくれるので, わざわざもう一度書きたくない。

 - クエリが変態すぎる。<code>Op.gt</code> とか <code>Op.or</code> とか書きたくない。



## ▲ Prisma

非常にメジャー. TypeScript が前提.

Prisma schema ファイルに定義を記述.
```
model User {
  id        Int      @id @default(autoincrement())
  createdAt DateTime @default(now())
  email     String   @unique
  name      String?
}
```

公式ドキュメントクエリの書き方、本当? `gt:`, `lte:` とか諸々, ちょっとこれは嫌だ。

```javascript
const users = await prisma.user.findMany({
  where: {
    OR: [
      {
        name: { startsWith: 'E', }, },
      {
        AND: {
          profileViews: { gt: 0, },
          role: { equals: 'ADMIN', }, }, },
    ],
  },
})
```



## ▲ TypeORM

TypeScript が前提。Decorator でフィールドなどを指定。しかし、デコレータがまだ JavaScript で標準化されておらず、TypeScript でも実験的機能のまま。仕様の安定性にやや不安あり。

[2024-11] 古いデコレータに依存しており、今から手を出しがたい。



## Bookshelf.js

[2023-04] Bookshelf.js は, 最終リリースから3年が経過しており、開発終了か? -- knex とヴァージョン不整合。もう使えない。
  peer knex@">=0.15.0 <0.22.0" from bookshelf@1.2.0


これは優れている. <i>Knex</i> query builder を利用している。Knex.js が優れているのが大きい。

<i>Ghost</i> headless-cms のストレージとして採用例あり。https://ghost.org/docs/architecture/

`bookshelf-modelbase` plugin を導入してもよい。よく使うカラムやメソッドを追加してくれる。
 - モデルにタイムスタンプを追加。`created_at` and `updated_at`
 - バリデーション. Joi を利用.
 - CRUDメソッド. `findAll`, `findOne`, `findOrCreate`, `create`, `update`, and `destroy`



このサンプルでは, 素の <i>Bookshelf.js</i> を利用している。



## Objection.js

これも Knex.js 上に作られた ORM. まあまあ人気があるようだ。

https://vincit.github.io/objection.js/ -- An SQL-friendly ORM for Node.js

```javascript
const middleAgedJennifers = await Person.query()
  .select('age', 'firstName', 'lastName')
  .where('age', '>', 40)
  .where('age', '<', 60)
  .where('firstName', 'Jennifer')
  .orderBy('lastName');
```

