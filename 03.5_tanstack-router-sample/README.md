
# TanStack Router sample

<a href="https://tanstack.com/router/">TanStack Router</a> sample

File-based route project generation:
```
  $ yarn create @tanstack/router
```


Not yet:
 - https://leonardomontini.dev/tanstack-router-authenticated-guards/
 