import * as React from 'react'
import { createFileRoute, Link, useNavigate } from '@tanstack/react-router'

export const Route = createFileRoute('/posts_/$postId/edit')({
  component: PostsEdit,
})

export default function PostsEdit() {
  const { postId } = Route.useParams()
  const navigate = useNavigate()

  const handleSubmit = (event) => {
    event.preventDefault()
    navigate({ to: '/posts/$postId', params: { postId: postId } })
  }

  return (
    <div className="p-2">
      <h3>Edit {postId}</h3>
      <button onClick={handleSubmit}>Update!</button>
    </div>
  )
}
