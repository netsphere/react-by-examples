
import * as React from 'react'
import { createFileRoute, Link, useNavigate } from '@tanstack/react-router'

export const Route = createFileRoute('/posts/$postId')({
  component: PostsShow,
})

function PostsShow() {
    const { postId } = Route.useParams();
    const navigate = useNavigate();

    const handleDelete = () => {
	if (window.confirm("OK?")) {
	    navigate({to:"/posts/"});
	}
    };
    
  return (
    <div className="p-2">
      <h3>Post {postId}</h3>
      <p>`src/routes/posts/$postId.jsx`</p>
      <Link to="/posts/$postId/edit" params={{postId:postId}}>
      Edit...</Link>
	<button onClick={handleDelete}>Delete</button>
    </div>
  )
}

