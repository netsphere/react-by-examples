
import * as React from 'react'
import { createFileRoute, Link } from '@tanstack/react-router'

export const Route = createFileRoute('/posts/')({
  component: PostsIndex,
})

function PostsIndex() {
  return <>
    <h3>Posts</h3>
      
    <div className="p-2">
	<Link to="1">Post 1</Link>
	<Link to="2">Post 2</Link>
    </div>
  </>;
}
