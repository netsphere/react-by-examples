"use strict";

// SuperTest: HTTP assertions made easy via superagent.
// See https://github.com/visionmedia/supertest/
// -> 依存が少ない。活発な開発が続いている

var app = require("../server");
var request = require("supertest").agent(app.listen());

describe("Errors", function() {
  it("should return 404", function(done) {
    request.get("/urlThatDoesNotExist")
    .accept("json")
    .expect(404)
    .end(done);
  });
});
