'use strict';

exports.up = function(knex) {
    return knex.schema.raw(
        'CREATE TABLE counts ( ' +
        '  id      INTEGER PRIMARY KEY AUTOINCREMENT, ' +
        '  user_id INTEGER NOT NULL REFERENCES users (id), ' +
        '  value   INTEGER NOT NULL, ' +
        '  created_at TIMESTAMP NOT NULL, ' +
        '  updated_at TIMESTAMP NOT NULL)' );
};

exports.down = function(knex) {
    return knex.schema.dropTable('counts');
};
