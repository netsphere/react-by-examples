'use strict';

// @note return するのを忘れないように!
exports.up = function(knex) {
    return knex.schema.raw(
        'CREATE TABLE users(  ' +
        '  id INTEGER PRIMARY KEY AUTOINCREMENT, ' +
        '  email VARCHAR(200) NOT NULL UNIQUE, ' +
        '  admin_role  BOOLEAN NOT NULL, ' +  // BOOLEAN は SQL99.
        '  password VARCHAR(200) NOT NULL, ' +
        '  created_at TIMESTAMP NOT NULL, ' +
        '  updated_at TIMESTAMP NOT NULL)');
};

exports.down = function(knex) {
    return knex.schema.dropTable('users');
};
