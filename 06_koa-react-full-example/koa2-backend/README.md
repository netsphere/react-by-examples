
Koa1 と Koa2 はまったく別物。ほとんど全部の書き直しになる。これでは Express からの乗り換えは厳しい。



## How to run

```
  $ npm install
  $ npx knex migrate:latest
  $ npm start
```


巻き戻す
```
$ npx knex migrate:rollback
```





## MongoDB + Mongoose (失敗)

このサンプルはデータベースとして MongoDB を使う。
MongoDBはヴァージョン4.2 から ACID特性のトランザクションをサポートしており、アプリケィションの永続データの保管先としても使える。

MongoDB の導入は、リポジトリを追加するところから。Fedora 34 はパッケージがないので、公式リポジトリを使う。https://docs.mongodb.com/manual/administration/install-on-linux/

<code>/etc/yum.repos.d/mongodb-org-5.0.repo</code> ファイル. 公式ドキュメントでは <code>$releasever</code> という変数を使っているが, Fedora だと 34 になってしまうため、手書きにする。

<pre>
[mongodb-org-5.0]
name=MongoDB Repository
baseurl=https://repo.mongodb.org/yum/redhat/8/mongodb-org/5.0/x86_64/
gpgcheck=1
enabled=1
gpgkey=https://www.mongodb.org/static/pgp/server-5.0.asc
</pre>

次のパッケージがある。

<pre>
mongocli.x86_64                               1.19.0-1             mongodb-org-5.0
mongodb-database-tools.x86_64                 100.5.0-1            mongodb-org-5.0
mongodb-mongosh.x86_64                        1.0.5-1.el7          mongodb-org-5.0
mongodb-org.x86_64                            5.0.2-1.el8          mongodb-org-5.0
mongodb-org-database.x86_64                   5.0.2-1.el8          mongodb-org-5.0
mongodb-org-database-tools-extra.x86_64       5.0.2-1.el8          mongodb-org-5.0
mongodb-org-mongos.x86_64                     5.0.2-1.el8          mongodb-org-5.0
mongodb-org-server.x86_64                     5.0.2-1.el8          mongodb-org-5.0
mongodb-org-shell.x86_64                      5.0.2-1.el8          mongodb-org-5.0
mongodb-org-tools.x86_64                      5.0.2-1.el8          mongodb-org-5.0
</pre>

```shell
  # yum install mongodb-org
```

=> <code>/usr/libexec/platform-python</code> が見つからず、失敗。RHEL 8 には存在するが、Fedora 34 にはパッケージも含めて存在しない。Fedora 27辺りで巻き戻された。https://fedoraproject.org/wiki/Changes/Platform_Python_Stack

```shell
  # ln -s /usr/bin/python3 /usr/libexec/platform-python
```
 -> アカン

それ以外の互換性問題もあり、Fedora 34 にはインストールできない。
See https://stackoverflow.com/questions/68617654/error-problem-nothing-provides-usr-libexec-platform-python-needed-by-mongodb
https://jira.mongodb.org/browse/SERVER-58870

MongoDB v4.2 まで戻してみてもアカンわ。



