"use strict";
// passport はストラテジを組み合わせる。
var LocalStrategy = require("passport-local").Strategy;
const User = require("../src/models/user");

var serialize = function(user, done) {
    if (!(user instanceof User))
        throw new TypeError('user must be User');
  done(null, user.id);
};

// ctx.req.user に格納される。ctx.request ではなく, ctx.req. ものすごい紛らわし
// い
var deserialize = async function(id, done) {
    let u = await User.query().findById(id); //.fetch();
    if (u) {
        delete u.password;
        done(null, u);
    }
    else
        done(null, false);
};

module.exports = function(passport) {
    passport.serializeUser(serialize);
    passport.deserializeUser(deserialize);
    passport.use( new LocalStrategy(User.authenticate) );
};
