"use strict";
var KoaRouter = require("koa-router");

var countController = require("../src/controllers/count");
var indexController = require("../src/controllers/index");
var authController = require("../src/controllers/account");

function loginRequired(ctx, next) {
    if ( ctx.isAuthenticated() ) {
        return next();
    }
    ctx.throw(403, {}); // ログインが必要 => 403 Forbidden
}

module.exports = function(app, passport) {
  // register functions
    var router = new KoaRouter({
        prefix:'/api'
    });

    router.get("/", indexController.index);

    router.get("/account", loginRequired, authController.getCurrentUser);
    router.post("/account", authController.createUser);
    // update は PUT /account が通常.

    router.post("/account/sign_in", authController.signIn);
    // sign_out は DELETE verb でもよい.
    router.post("/account/sign_out", loginRequired, authController.signOut);

    // secured routes
    router.get("/count", loginRequired, countController.getCount);
    router.post("/count/inc", loginRequired, countController.increment);
    router.post("/count/dec", loginRequired, countController.decrement);

    app.use(router.routes())
       .use(router.allowedMethods());

    return router;
};
