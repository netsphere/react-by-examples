"use strict";
const path = require("path");
const serve = require("koa-static-cache");
const session = require("koa-generic-session");
// 完全に廃れた.
// const MongoStore = require("koa-sess-mongo-store");
// 代替 => const MongoStore = require('koa-generic-session-mongo');

//const responseTime = require("koa-response-time");
const logger = require("koa-logger");

// co-views は廃れた.
// const views = require("co-views");
// => 代替は koa-views パッケージ. koa-views は consolidate パッケージを利用.
//    ejs, handlebars が双璧。そのずっと下に mustache, pug (旧jade) が続く.
//    ロジックをそのまま書ける ejs か, ロジックを書かせない Handlebars か.
//    => #if, #each, #with で分岐, ループは組める.
const views = require("koa-views");

const compress = require("koa-compress");

// koa-error は consolidate パッケージを利用. koa-views との組み合わせでOK.
// => しかし, APIモードでは、整形する必要なし.
// const errorHandler = require("koa-error");

const bodyParser = require("koa-bodyparser");

const STATIC_FILES_MAP = {};
const SERVE_OPTIONS = { maxAge: 365 * 24 * 60 * 60 };

module.exports = function(app, config, passport)
{
    if (!config.app.keys) {
        throw new Error("Please add session secret key in the config file!");
    }
    app.keys = config.app.keys;

  if (config.app.env !== "test") {
    app.use(logger());
  }

    //app.use(errorHandler());
    // 例外の場合に JSON でエラーメッセージを返す
    app.use(async (ctx, next) => {
        try {
            await next();
        }
        catch (error) {
            ctx.status = error.status || 500;
            ctx.type = 'json';
            ctx.body = {
                error: {
                    code: ctx.status,
                    message: error.message,
                    type: error.type,    }
            };
            ctx.app.emit('error', error, ctx);
        }
    });

// APIモードで静的ファイルの出番はないはず。
//   => 後段の koa-views だけ有効にすればいい。
//  if (config.app.env === "production") {
    app.use(serve(path.join(config.app.root, "/public"), SERVE_OPTIONS, STATIC_FILES_MAP));
/*
  }
    else {
    app.use(require("koa-proxy")({
      host: "http://localhost:2992",
      match: /^\/_assets\//,
    }));
  }
*/

  app.use(session({
      key: "koareactfullexample.sid",
      //store: new MongoStore({ url: config.mongo.url }),
  }));

  app.use(bodyParser());
  app.use(passport.initialize());
  app.use(passport.session());

    // koa-views
    // 個々の表示は, ルーティングのなかで render() する.
    app.use( views(config.app.root + "/views", {
        map: {
            html: "handlebars" },
    }) );

  app.use(compress());
  //app.use(responseTime());
};
