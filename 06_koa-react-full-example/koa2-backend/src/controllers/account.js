"use strict";

const bcrypt = require('bcrypt');
const passport = require("koa-passport");
const User = require("../models/user");

// セッションID固定化攻撃 / Session fixation attack 対策.
const reset_session_id = async (ctx) => {
  const cur = ctx.session;
  await ctx.regenerateSession();  // セッションIDが替わる.
  for (let k in cur)
    ctx.session[k] = cur[k];
  await ctx.saveSession();
}

// passport.deserializeUser() で設定されている。
function current_user(ctx) {
    if ( !ctx.req.user ) // ctx.request ではない!
        return null;
    return ctx.req.user;
}

// POST /account/sign_in
exports.signIn = async (ctx, next) => {
    return passport.authenticate("local", async (err, user) => {
        if (!user) {
            ctx.throw(401, err); // ログイン失敗時は 401 Unauthorized.
            return;
        }

        ctx.login(user);
        await reset_session_id(ctx);
        ctx.body = user;
    })(ctx);
};

// GET /account
exports.getCurrentUser = async (ctx, next) => {
    console.log(ctx.session);
    console.log(ctx.session.passport);

    ctx.body = { user: current_user(ctx) };
    ctx.status = 200;
};

// POST /account/sign_out
exports.signOut = async (ctx, next) => {
    ctx.logout();
    await ctx.regenerateSession(); // 重要!
    ctx.status = 204; // 205 Reset Content の流派もある?
};

// POST /account
// ctx.request.body:
//   {
//     username: 'hori@localhost',
//     password: '1234',
//     password_confirmation: '1234'
//   }
exports.createUser = async (ctx, next) => {
    if ( !ctx.request.body) {
        return ctx.throw(400, "The body is empty");
    }

    if (!ctx.request.body.username) {
        return ctx.throw(400, "Missing username");
    }
    if (!ctx.request.body.password) {
        return ctx.throw(400, "Missing password");
    }
    if (ctx.request.body.password !== ctx.request.body.password_confirmation) {
        return ctx.throw(400, "confirmation mismatch");
    }

    // Objection.js は ActiveRecod とはまったく異なる
    let user; // = new User();
    try {
        user = await User.query().insert({ // await user.save({
            email:  ctx.request.body.username,
            admin_role: Number(ctx.request.body.admin_role) || 0,
            password: bcrypt.hashSync(ctx.request.body.password, 5),
        });

        // 流れでログインする
        ctx.login(user);
        await reset_session_id(ctx);
    }
    catch (err) {
        console.log(err);
        return ctx.throw(400, "Save error");
    }

    ctx.status = 200;
    delete user.password;
    ctx.body = { user: user };
};
