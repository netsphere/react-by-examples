"use strict";

const Count = require("../models/count");

// GET /count
exports.getCount = async (ctx, next) => {
    console.log(ctx.req.user.id); // DEBUG DEBUG
    let count = await Count.query().findOne({user_id: ctx.req.user.id}); //.fetchAll();
    console.log(count);
    if ( !count ) {
        count = await Count.query().insert({
            user_id: ctx.req.user.id,
            value: 0,      });
        //await count.save();
    }
    ctx.body = { count: count.value };
    ctx.status = 200;
};

exports.increment = async (ctx, next) => {
    let count = await Count.query().findOne({user_id: ctx.req.user.id}); //.fetch();
    console.log(count);
    if ( !count ) {
        ctx.throw(404, "Missing");
        return;
    }

    count.value = count.value + 1;
    await Count.query().update({value: count.value}).where('id', count.id);
    ctx.body = { count: count.value };
    ctx.status = 200;
};

exports.decrement = async (ctx, next) => {
    let count = await Count.query().findOne({user_id: ctx.req.user.id}); //.fetch();
    console.log(count);
    if ( !count ) {
        ctx.throw(404, "Missing");
        return;
    }

    count.value = count.value - 1;
    await Count.query().update({value:count.value}).where('id', count.id);
    ctx.body = { count: count.value };
    ctx.status = 200;
};
