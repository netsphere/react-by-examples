"use strict";
/**
 * Dependencies
 */
const fs = require("fs"); // Node.js
const path = require("path");
const Koa = require("koa");
const passport = require("koa-passport");

const config = {
    app: {
        keys: ["super-secret"],  // TODO: 環境変数から設定
        env: (process.env.NODE_ENV || "development"),
        root: path.normalize(path.join(__dirname, "/..")),
        port: 3001,
    }
}

/**
 * Load the models
 */
const modelsPath = config.app.root + "/src/models";
fs.readdirSync(modelsPath).forEach(function(file) {
  if ( file.endsWith(".js") ) {
    require(modelsPath + "/" + file);
  }
});

/**
 * Server
 */
const app = module.exports = new Koa(); // For Koa v1, `var app = koa();`

require("../config/passport")(passport);

require("../config/koa")(app, config, passport);

// Routes
const router = require("../config/routes")(app, passport);

// DEBUG Routings を全部表示.
console.log(router.stack.map(i => i.methods + " " + i.path));

// Start app
if (!module.parent) {
  app.listen(config.app.port);
  console.log("Server started, listening on port: " + config.app.port);
}
console.log("Environment: " + config.app.env);
