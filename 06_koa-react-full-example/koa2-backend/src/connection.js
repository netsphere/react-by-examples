'use strict';

// 設定ファイル名は `/knexfile.js` でなければならない.

// Initialize knex.
const dbconn = require('knex')(
    require(__dirname + '/../knexfile')[process.env.NODE_ENV || 'development'])

// Give the knex instance to Objection.
const { Model } = require('objection');
Model.knex(dbconn);

module.exports = Model;
