"use strict";

const {Model} = require('objection');
const User = require("./user");

/**
 * Schema
 */
class Count extends Model
{
    static tableName = 'counts';

    // Timestamps
    $beforeInsert() {
        const now = new Date().toISOString();
        this.created_at = now; this.updated_at = now;
    }
    $beforeUpdate() { this.updated_at = new Date().toISOString(); }

    // Relationship定義
    static get relationMappings() {
        const User = require('./user');

        return {
            user: {
                relation: Model.BelongsToOneRelation,
                modelClass: User,
                join: {
                    from: 'counts.user_id', to: 'users.id' } }
        };
    }
}


module.exports = Count;
