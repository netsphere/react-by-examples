"use strict";

const bcrypt = require("bcrypt"); // version that supports yields
const Model = require('../connection');

class User extends Model
{
    static tableName = 'users';

    // Timestamps
    $beforeInsert() {
        const now = new Date().toISOString();
        this.created_at = now; this.updated_at = now;
    }
    $beforeUpdate() { this.updated_at = new Date().toISOString(); }

    // Relationship定義
    static get relationMappings() {
        const Count = require('./count');

        return {
            counts: {
                relation: Model.HasManyRelation,
                modelClass: Count,
                join: {
                    from: 'users.id', to: 'counts.user_id' } }
        };
    }
}


// LocalStrategy() に渡す 3引数のメソッド.
User.authenticate = async function(username, password, done) {
    console.log("Challenge: '" + username + "' and '" + password + "'"); // DEBUG DEBUG

    const u = await User.query().findOne({email: username}); //.fetch();
    if (!u) {
        return done( {field:'username', message:'User not found'}, false );
    }

    console.log('u.password = ', u);
    if ( bcrypt.compareSync(password, u.password) ) {
        // 成功
        delete u.password;
        return done(null, u);
    }
    else {
        return done( {field:'password', message:'Password did not match'},
                         false );
    }
}


module.exports = User;
