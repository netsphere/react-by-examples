
# koa-react-full-example

Full example using Koa2, React, Passport, <s>Mongoose</s><s>Bookshelf.js ORM &amp;</s>Knex, and SQLite3.

Based on https://github.com/dozoisch/koa-react-full-example/
動くようにするのが、またえらい大変だった。

Passport で認証をおこなう。

This example contains a MVC pattern that presents a simple counter to the client that increments and decrements a value in the Mongo Database with Mongoose. The UI is all handled with ReactJS. It uses the yield keyword from ES6.

Frontend 側では、ログイン状態を内側のコンポーネントに引き回すのに, `useContext` を用いている。




## パッケージ選定

### Backend

[2023-04] Bookshelf.js は, 最終リリースから3年が経過しており、開発終了. -- knex とヴァージョン不整合。もう使えない。
```
npm ERR! Could not resolve dependency:
npm ERR! peer knex@">=0.15.0 <0.22.0" from bookshelf@1.2.0
npm ERR! node_modules/bookshelf
npm ERR!   bookshelf@"^1.2.0" from the root project
```
移行先は, <a href="https://vincit.github.io/objection.js/">Objection.js</a> が良さそう。

Koa2 はパッケージが細分化されており、ちゃんとした組み合わせを選ぶのが一苦労。次の組み合わせでいいようだ。

 - "koa": "^2.13.1",
 - "koa-bodyparser": "^4.3.0",
 - "koa-compress": "^5.0.1",
 - "koa-generic-session": "^2.1.1",    `koa-session` はセッションIDの差し替えができず、使用不可。
 - "koa-logger": "^3.2.1",
 - "koa-passport": "^4.1.4",
 - "koa-router": "^10.0.0",      `koa-route` は廃れた.
 - "koa-static-cache": "^5.1.4",
 - "koa-views": "^7.0.1",

[2023-05]
koa-passport v5.0.0 以降で Passport v0.6 に依存。koa-passport v4.1.3 は Passport v0.4 に依存。
Passport v0.6 で, ログイン時にセッションを再生成するようになったが、これがエラーを発生。これに対し、koa-passport で対策するのではなく、koa-session で対策。koa-generic-session が使えなくなった。
```
TypeError: req.session.regenerate is not a function
```

このサンプルでは, <code>"koa-passport": "^4.1.4"</code> とした。セッション再生成は、手書きする。<kbd>npm audit</kbd> で次の警告が出るが、やむなし。
```
$ <kbd>npm audit</kbd>
passport  <0.6.0
Severity: moderate
Passport before 0.6.0 vulnerable to session regeneration when a users logs in or out - https://github.com/advisories/GHSA-v923-w3x8-wh69
fix available via `npm audit fix --force`
Will install koa-passport@6.0.0, which is a breaking change
```


### Frontend

react-router が非互換な破壊的変更が多すぎて、つらすぎる。
react-router v1.0 と, v4 がヒドい。v6 も完全に書き直しになる。アホか




## How to run

```
  $ npm install
  $ npm start
```



## Topic: React でグローバルな状態を引き回す

例えばログイン状態、カラーテーマなど。

まず, その状態を必要とするもっとも外側のコンポーネントの state として状態を持たせる。

内側のコンポーネントに状態を伝えるのに props を使うと、引き回すだけのコードが必要になってしまう。特に、間にその状態を必要としないようなコンポーネントが挟まっている場合、不必要に複雑になる。

状態を必要とする内側のコンポーネントで, ピンポイントで `useContext()` を使えばよい。



## Topic: Protected route

React Router v6 で書き方が変わった。検索でよく引っ掛かるのは、次の二つだが、二つとも罠に引っ掛かっている。

 - <a href="https://zenn.dev/longbridge/articles/61b05d8bdb014d">React:React Router v6 で 認証されていないユーザーや権限がないユーザーをリダイレクトする</a> `<RouteAuthGuard />`
 - <a href="https://www.robinwieruch.de/react-router-private-routes/">React Router 6: Private Routes (alias Protected Routes)</a>

次のルーティングを考える。`/account` のように特定のページを守りたい場合と, `/admin` 以下の子のルートも含めて守りたい場合の、両方に対応したい。

```javascript
  <Routes>
    <Route exact path="/" element={<IndexPage />} />
    <Route       path="/account/sign_in" element={<AccountSignInPage />} />
    <Route       path="/account/sign_up" element={<AccountSignUpPage />} />
    <Route exact path="/account"
           element={<ProtectProvider component={<AccountProfilePage />} />} />
    <Route path="/admin"
           element={<ProtectProvider allowRoles={[RoleType.Admin]} />} >
      <Route index         element={<AdminIndexPage />} />
      <Route path="page2" element={<AdminPage2Page />} />
    </Route>

    <Route       path="/*" element={<NotFoundPage />} />
  </Routes> </> )}
```

前者の場合は `props.component` で渡されるので、それを render すればよい。後者の場合, `props.component` でもなく, `children` *でもない*!

React Router v5 では `props.children` として子要素が渡されていたが, v6 では `undefined` になってしまう。`<Outlet />` を使わなければならない。どうしてそうした..




