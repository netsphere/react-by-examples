
// Object.observe() が Harmony で提案されたが、2015年11月に取り下げられた。
// C# の IObserver<T> は, クラスを登録するやり方. Java も同様。コールバックされ
// るメソッド名は異なる。
// 簡単に, 関数を登録するようにしよう.
const observableMethods = {
    subscribe: function(observer) {
        console.log("subscribe() enter");
        if (observer === null || observer === undefined)
            throw new TypeError("observer is needed");

        // Set (ES2015) を使う.
        if ( !this.hasOwnProperty('_observers') )
            this._observers = new Set();

        this._observers.add(observer);
    },

    unsubscribe: function(fn) {
        if ( !this.hasOwnProperty('_observers') )
            this._observers = new Set();

        this._observers.delete(fn);
    },

    notifyObservers: function(data) {
        console.log('notifyObservers() enter');
        console.log(this._observers);

        for (let obs of this._observers)
            obs(data);
    }
};

export default observableMethods;
