//import logo from './logo.svg';
import React, {useState, useEffect} from 'react';
// react-router v3 の <Router history={hashHistory}> は廃止.
// <Switch> は react-router v4 で追加され, v6 でまた廃止。<Routes> に変更.
import { BrowserRouter, Route,
         Routes,
         Link, useNavigate, useLocation } from 'react-router-dom';
// React Router v6: `withRouter` は単に廃止
//import { withRouter } from 'react-router';

import './App.css';

import IndexPage from "./pages/index";
import AdminIndexPage from "./pages/admin/index";
import AdminPage2Page from "./pages/admin/page2";
import AccountSignInPage from "./pages/account/sign_in";
import AccountSignUpPage from "./pages/account/sign_up";
import AccountProfilePage from "./pages/account/profile";
// 404 error
import NotFoundPage from "./pages/not_found";
import {AuthStore, AuthUserContext, ProtectProvider, RoleType} from './components/auth_user_provider';
import NavItem from './components/NavItemRoute';


// - Nested <Route /> は react-router v4 で廃止。(v6 で復活予定)
// - React Router v6 は Reach Router に片寄せ. 多くの破壊的変更がある。
//     See https://reacttraining.com/blog/reach-react-router-future/
//   `useLocation`, `useNavigate`, `useParams` は Reach Router 由来.
//   Reach Router: https://reach.tech/router/    .. 2020年2月の v1.3.1 が最新

function App()
{
    // react-router v6: リダイレクトのやり方も変わった。
    const navigate = useNavigate();
    const location = useLocation();

    // ログイン状態は, 通常, 一番外側である App に保存.
    const [state, setState] = useState({
        value: {currentUserId: null},
        isLoaded: false,
    });

    // Callback
    // @param user ログアウトしたときは null
    const onLoggedInChanged = (user) => {
        console.log('onLoggedInChanged(): set user = ' + JSON.stringify(user)); // DEBUG
        if ( state.value.currentUserId !== (user ? user.id : null )) {
            setState( {...state,
                       value: { currentUserId: user ? user.id : null},  });
        }
    };

    AuthStore.instance().subscribe(onLoggedInChanged);

    useEffect( () => { // async componentDidMount() {
        if ( !state.isLoaded ) {
            AuthStore.instance().currentUser().then( currentUserId => {
                setState( {...state,
                           value: { currentUserId: currentUserId },
                           isLoaded: true });
            });
        }
    });

    // Callback
    const signOutClicked = (event) => {
        AuthStore.instance().signOut()
            .then( () => {
                // Error: Invariant failed:
                //   You should not use <withRouter(App) /> outside a <Router>
                // this.props.history.push('/');
                navigate("/",
                         {state: {notice: "Logout!"}});
            });
    };

/*
<Route handler={...} /> は非常に古い書き方 (v0.13.x 時代)。廃れた。
<NotFoundRoute />, <DefaultRoute /> も react-router v1.0 で廃止.
 => v1.0.0 は 2015年11月リリース。これ以前の書き方。

<Link>, <Redirect /> も <Router> の中に入れなければならない。
 => 全体を <Router> or <BrowserRouter> で囲めばよい。

SPA では, Not Found ページも status 200 で返す.
<HashRouter> は http://localhost:3000/#/ というような URL になる.
 -> やはり, ページ遷移で state がクリアされる。
*/
    return (<>
<AuthUserContext.Provider value={state.value}>
  { !state.isLoaded ? (
  <div>Loading...</div>
  ) : (<>
  <nav className="navbar navbar-expand-sm navbar-light bg-light">
    <span className="navbar-brand">Counter</span>
    <button className="navbar-toggler" type="button" data-bs-toggle="collapse"
            data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false"
            aria-label="Toggle navigation">
      <span className="navbar-toggler-icon"></span>
    </button>
    <div className="collapse navbar-collapse" id="navbarSupportedContent">
      <ul className="navbar-nav me-auto">
        <NavItem exact to="/">Home</NavItem>
        <NavItem exact to="/admin">Admin</NavItem>

        {state.value.currentUserId && <a href="/account">{AuthStore.getUser(state.value.currentUserId).email}</a> }
        {state.value.currentUserId ?
         <button className="btn btn-outline-success" onClick={signOutClicked}>Log Out</button> :
         <>
           <Link to="/account/sign_up" className="nav-link">Register</Link>
           <Link to="/account/sign_in" className="nav-link">Sign In</Link>
         </> }
      </ul>
    </div>
  </nav>

  { location.state && location.state.alert ? <div className="alert alert-danger" role="alert">
                {location.state.alert}
              </div> : '' }
  { location.state && location.state.notice ? <div className="alert alert-success" role="alert">
                {location.state.notice }
              </div> : '' }

  <Routes>
    <Route exact path="/" element={<IndexPage />} />
    <Route       path="/account/sign_in" element={<AccountSignInPage />} />
    <Route       path="/account/sign_up" element={<AccountSignUpPage />} />
    <Route exact path="/account"
           element={<ProtectProvider component={<AccountProfilePage />} />} />
    <Route path="/admin"
           element={<ProtectProvider allowRoles={[RoleType.Admin]} />} >
      <Route index         element={<AdminIndexPage />} />
      <Route path="page2" element={<AdminPage2Page />} />
    </Route>

    <Route       path="/*" element={<NotFoundPage />} />
  </Routes> </> )}
</AuthUserContext.Provider> </> );
}


// Error: Invariant failed: You should not use <withRouter(App) /> outside a
// <Router>
// => 一つ外側で, <BrowserRouter> で囲む.
export default App;
