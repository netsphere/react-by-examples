
import React, {useContext} from 'react';
import {useLocation, Navigate, Outlet} from 'react-router-dom';
import Account from '../stores/account';
import observableMethods from '../observe';

export const RoleType = {
    Guest: 'guest',
    Admin: 'admin',
    User: 'user'
};

// ユーザの状態を保持する.
// コールバック関数を登録できる。
export class AuthStore
{
    static _instance = null;

    static instance() {
        if ( !AuthStore._instance ) {
            AuthStore._instance = new AuthStore();
        }
        return AuthStore._instance;
    }


    constructor() {
        this._init = false;
        this._current_user = null;
    }

    isLoggedIn() {
        return this.currentUser() !== null;
    }

    // 現在のサーバのログイン状態を問い合わせる。
    // @return id のみ. 未ログインの時は null
    async currentUser() {
        console.log("currentUser() current = " + JSON.stringify(this._current_user) + ', init = ' + this._init);
        if ( !this._init ) {
            // {"user":{"id":1,"email":"hori@localhost","password":null}}
            let user = null;
            try {
                let ret = await Account.find(); // 必ず待つ
                user = ret.user;
            }
            catch (err) {
                user = null;
                //throw err; 握りつぶす
            }
            this._setCurrentUser(user);  // notify が走る
        }
        return this._current_user ? this._current_user.id : null;
    }


    static getUser(user_id) {
        const currentUser = AuthStore.instance()._current_user;
        return currentUser && user_id ?
               (currentUser.id === user_id ? currentUser : null) : null;
    }

    // Private. 変更の場合はコールバックする.
    // @param user   null の場合も、サーバからはログアウトしない。
    _setCurrentUser(user) {
        console.log("_setCurrentUser() set " + JSON.stringify(user));

        let changed = false;
        if ( !this._current_user ) {
            if ( user )
                changed = true;
        }
        else {
            if ( !user || (this._current_user.id !== user.id) )
                changed = true;
        }
        this._current_user = user;
        this._init = true;

        if (changed)
            this.notifyObservers(user);
    }

    // Create
    async register(body) {
        if ( body === null || body === undefined)
            throw new TypeError("body");

        let user = await Account.create(body);
        this._setCurrentUser(user);
    }

    async signIn( body ) {
        if ( body === null || body === undefined)
            throw new TypeError("body");

        let user = await Account.signIn(body);
        this._setCurrentUser(user);
    }

    async signOut() {
        await Account.signOut();
        this._setCurrentUser(null);
    }
}
// Mixin
Object.assign(AuthStore.prototype, observableMethods);


/*
  グローバルな状態. 大文字でなければならない。Component 名になるため。
  再render するかどうかは, value が `Object.is()` で判定される
    -> オブジェクトだと同じリファレンスのときのみ true になる (同じ値ではない)。
       公式ドキュメントだとオブジェクトを渡している例もある。いいのか?

公式ドキュメントでは, value のために毎回新規オブジェクトを生成してはならない。次のようにしろ、となっている。
```
class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: {something: 'something'},
    };
  }

  render() {
    return (
      <MyContext.Provider value={this.state.value}>
        <Toolbar />
      </MyContext.Provider>
    );
  }
}
```
*/
export const AuthUserContext = React.createContext({
    currentUserId: null
});


// react-router v4 & v5 では, 次の二つの独自コンポーネントの作り方がある;
//     a) <Route /> を囲むようなコンポーネント
//        例 https://qiita.com/soutaito/items/691ac9dabe765e98d9f9
//     b) <Route /> の代わりになるようなコンポーネント.
//    どちらも作れるが, <Switch> との意味の兼ね合いや, あるコントローラのなか
//    でも要認証とそうでない route があったりするので, 後者のほうがよさそうか.
export function ProtectProvider( props )
{
    console.log('props= ', props); // DEBUG

    const location = useLocation(); // redirect back 用.
    const authUser = AuthStore.getUser(useContext(AuthUserContext).currentUserId);
    console.log("ProtectProvider: user = " + JSON.stringify(authUser));

    let allow = false; // 未ログインのときはブロック.
    if (authUser) {
        if ( !props.allowRoles ) {
            allow = true;
        }
        else {
            if (props.allowRoles.includes('admin') && authUser.admin_role)
                allow = true;
        }
    }

    if ( !allow ) {
        return <Navigate to="/account/sign_in"
                          state={{from: location, // Objectなのに注意!
                                  alert: 'Please sign-in!'}} />;
    }

    console.log('children = ', props.children ); // DEBUG => undefined
    console.log('component = ', props.component ); // DEBUG => あり
    return props.component ? props.component : <Outlet />;
}


//export default AuthStore;
