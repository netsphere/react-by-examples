
import React, { useContext, useState, useEffect } from "react";
//import request from "superagent";
//import { Button, Badge } from "react-bootstrap";
import {AuthStore, AuthUserContext, ProtectProvider, RoleType} from './auth_user_provider';
import Count from '../stores/count';


function Counter() // extends React.Component {
{
    const currentUserId = useContext(AuthUserContext).currentUserId;

    const [state, setState] = useState({
        count: 0,
        isLoaded: false,
        error: null });

    useEffect( () => { // componentDidMount() {
        if (!state.isLoaded) {
            Count.find().then( count => {
                console.log(count);
                setState({...state,
                          count: count.count, isLoaded:true });
            }).catch( err => {
                setState({...state,
                          error: err.response.error, isLoaded:true });
            });
        }
    });

    const onClickInc = (event) => {
        event.preventDefault();
        Count.inc().then( count => {
            setState({...state,
                      count: count.count });
        }).catch( err => {
            setState({...state,
                      error: err.response.error });
        });
    };

    const onClickDec = (event) => {
        event.preventDefault();
        Count.dec().then( count => {
            setState({...state,
                      count: count.count });
        }).catch( err => {
            setState({...state,
                      error: err.response.error });
        });
    };

    return (<>
  {state.error && <div class="alert alert-danger" role="alert">
                    Error: {state.error.message}</div> }
  { !state.isLoaded ? (
  <div>Loading...</div>
  ) : (
  <div>
    <h3>Counter</h3>
    <div className="counter">
      Count
      <span className="badge bg-secondary">{state.count}</span>
      <button className="btn btn-success" onClick={onClickInc}>Increment</button>
      <button className="btn btn-danger" onClick={onClickDec}>Decrement</button>
    </div>
  </div>
  )}
            </>);
}


export default Counter;
