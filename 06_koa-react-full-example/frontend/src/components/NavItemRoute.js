// -*- coding:utf-8 -*-

import PropTypes from 'prop-types';
import { Link, useLocation } from 'react-router-dom';

// Bootstrap は, 親要素 li のほうに active を付ける
//   => Bootstrap5: .nav-link のほうに active 付けるようになった.
// From https://stackoverflow.com/a/43263057
export default function NavItem({children, to, exact}) {
    const location = useLocation();
    //=> {pathname: '/articles/', search: '', hash: '', state: null, key: '5i20du6q'}
    const match = location.pathname === to;
    return <li className="nav-item">
        {match ? <span className="nav-link active">{children}</span> :
                 <Link to={to} className="nav-link">{children}</Link> }
      </li> ;
}
NavItem.propTypes = {
    to:    PropTypes.string.isRequired,
    exact: PropTypes.bool,
    children: PropTypes.node.isRequired,
};
