
import API from './api';

// Singleton resource パタン
// API にアクセスするだけの class.
class Account extends API
{
    // override
    static resourceName = 'account';

    // override
    static async find() {
        const res = await API.apicall('GET', `/${Account.resourceName}`);
        return API._body_or_error(res, "find() error");
    }
/*
    // @param body フィールド値. username, password, password_confirmation
    static async create( body ) {
        const res = await API.apicall('POST', `/${Account.resource_name}`, body);
        return (await API._body_or_error(res, "create() error"));
    }
*/

    // @param body フィールド値. username, password, remember
    static async signIn( body ) {
        const res = await API.apicall('POST', `/${Account.resourceName}/sign_in`, body);
        return API._body_or_error(res, "signIn() error");
    }

    static async signOut() {
        return await API.apicall('POST', `/${Account.resourceName}/sign_out`);
    }
}


export default Account;
