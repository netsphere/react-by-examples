
import API from './api';

// Singleton resource パタン
class Count extends API
{
    // override
    static resourceName = 'count';

    // override
    static async find() {
        const res = await API.apicall('GET', `/${Count.resourceName}`);
        return API._body_or_error(res, "find() error");
    }

    static async inc() {
        const res = await API.apicall('POST', `/${Count.resourceName}/inc`);
        return API._body_or_error(res, "inc() error");
    }

    static async dec() {
        const res = await API.apicall('POST', `/${Count.resourceName}/dec`);
        return API._body_or_error(res, "dec() error");
    }
} // class Count

export default Count;
