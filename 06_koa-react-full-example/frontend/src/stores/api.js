
// Client-side

import ApiError from './api_error';

// APIサーバのエンドポイント. このサンプルでは, Reactアプリの origin と同じにす
// ること (ポートが違っても cross-origin になる).
// Cross-origin 対策としては,
//   a) CORS (Cross-Origin Resource Sharing) プロトコルを実装, または
//   b) Same-origin になるようにする;
//      - 開発環境 package.json に proxy を設定.
//      - 本番環境 Web server の reverse proxy 設定。
const ENDPOINT = '/api';


class API
{
    // static method のなかで this は レシーバ (=APIの子クラス) になる。
    // これで、どのクラスか判別できる。
    // Next.js: 本番環境ではトランスパイルされて、クラス名が変わる。
    //          明示的にリソース名を指定すること。複数形と単数形の両方に対応できる。
    static resourceName = "to_be_overridden";

    // async function は Promise を返す. 例外も Promise に変換される.
    // @return HTTP status code が 200..299 の場合, response そのもの.
    //         それ以外の場合, 例外 ApiError
    static async apicall(method, url, body) {
        // 現代では, XMLHttpRequest は古く、使われない.
        // fetch() の戻り値は Promise<Response>
        // network error は例外 TypeError. それ以外は .then() で受ける.
        // return を忘れずに.
        return fetch(`${ENDPOINT}${url}`, {
            method: method,
            body:   JSON.stringify(body),
            headers: {
                // application/json のやりとりは CORS 対応が必要.
                'content-type': 'application/json',
                accept: 'application/json'  },
        }).then( async (res) => {
            if ( res.status >= 200 && res.status <= 299 ) {
                if ( res.status === 204 )
                    return null;
                return res; //.json();
            }
            else {
                const res_body = res.json ? await res.json() : {};
                throw new ApiError(res.status, "Api call error", res_body);
            }
        });
    }


    // 複数リソースのみ。
    // @param filter Article.find_all({page: event.selected + 1}).then( data => {
    static async find_all(filter) {
        let url = this.resourceName.toLowerCase();
        if (filter !== null && filter !== undefined) {
            url += "?" + Object.entries(filter).map(x => `${x[0]}=${x[1]}`).join('&');
        }
        const response = await API.apicall('GET', "/" + url);
        return response.json ? response.json() : {};
    }


    static async _body_or_error(response, error_default_msg) {
        const res_body = response && response.json ? await response.json() : {};
        if (res_body.error) {
            throw new ApiError(response.status,
                               res_body.error.message || error_default_msg,
                               res_body);
        }
        return res_body;
    }

    // @param filter 現状, id のみ受け付ける.
    static async find(filter) {
        const id = Number(filter);
        if ( !(id > 0) )
            throw new RangeError(":id must be aNumber");
        //const model_name = this.name.toLowerCase() + "s";
        const response = await API.apicall('GET',
                                           "/" + this.resourceName + `/${id}`);
        // throw {foo:"bar", ...} のように、ただのオブジェクトを投げることもで
        // きるが, それでは型がなくなるので、別途 ApiError クラスを定義する。
        return API._body_or_error(response, "find() error");
    }


    // @param item  item.id を id として投げる.
    static async create(item) {
        //const model_name = this.name.toLowerCase() + "s";
        const id = Number(item.id);

        let response;
        if ( id > 0 ) { // update
            response = await API.apicall('PUT',
                                    "/" + this.resourceName + `/${id}`, item);
        }
        else { // create
            response = await API.apicall('POST', "/" + this.resourceName, item);
        }

        return API._body_or_error(response, "create() error");
    }


    static async delete(filter) {
        //const model_name = this.name.toLowerCase() + "s";
        const id = Number(filter);
        if ( !(id > 0) )
            throw new RangeError(":id must be aNumber");

        return API.apicall('DELETE', "/" + this.resourceName + `/${id}`);
    }

} // class API

export default API;
