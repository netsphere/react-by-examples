
// HTTP Status Code
// See https://www.iana.org/assignments/http-status-codes/http-status-codes.xhtml
// -> 自分で作らずに, http-errors npm パッケージ辺りを使ったほうがよい。
const STATUS_CODES = {
    "100": "Continue",
    "101": "Switching Protocols",
    "102": "Processing",
    "103": "Early Hints",

    "200": "OK",
    "201": "Created",
    "202": "Accepted",
    "203": "Non-Authoritative Information",
    "204": "No Content",
    "205": "Reset Content",
    "206": "Partial Content",
    "207": "Multi-Status",
    "208": "Already Reported",
    "226": "IM Used",

    "300": "Multiple Choices",
    "301": "Moved Permanently",
    "302": "Found",
    "303": "See Other",
    "304": "Not Modified",
    "305": "Use Proxy",
    // 306 (Removed)
    "307": "Temporary Redirect",
    "308": "Permanent Redirect",

    "400": "Bad Request",
    "401": "Unauthorized",
    "402": "Payment Required",
    "403": "Forbidden",
    "404": "Not Found",
    "405": "Method Not Allowed",
    "406": "Not Acceptable",
    "407": "Proxy Authentication Required",
    "408": "Request Timeout",
    "409": "Conflict",
    "410": "Gone",
    "411": "Length Required",
    "412": "Precondition Failed",
    "413": "Content Too Large",  // RFC 9110
    "414": "URI Too Long",
    "415": "Unsupported Media Type",
    "416": "Range Not Satisfiable",
    "417": "Expectation Failed",
    // 418 (unused)  .. "I'm a teapot"
    "421": "Misdirected Request",
    "422": "Unprocessable Entity",
    "423": "Locked",
    "424": "Failed Dependency",
    "425": "Too Early",
    "426": "Upgrade Required",
    "428": "Precondition Required",
    "429": "Too Many Requests",
    "431": "Request Header Fields Too Large",
    "451": "Unavailable For Legal Reasons",

    "500": "Internal Server Error",
    "501": "Not Implemented",
    "502": "Bad Gateway",
    "503": "Service Unavailable",
    "504": "Gateway Timeout",
    "505": "HTTP Version Not Supported",
    "506": "Variant Also Negotiates",
    "507": "Insufficient Storage",
    "508": "Loop Detected",

    //"510": "Not Extended (OBSOLETED)",
    "511": "Network Authentication Required"
}

function toName(code) {
    // 名前が混乱しているので、整える。
    const code_class = code / 100;
    let s = STATUS_CODES[code.toString()];
    if ( code_class === 4 || code_class === 5 )
        s = s.replace(/error$/i, '').trim() + " ERROR";

    return s.toUpperCase();
}


class ApiError extends Error
{
    // @param extras エラーオブジェクトの属性を生やす
    // @param response body部分.
    // コンストラクタは async function にできない
    constructor(code, message, response, extras) {
        if (code < 100 || code > 599)
            throw new RangeError("`code` must be between 100 and 599");

        // fileName, lineNumber は Firefox のみ.
        super(message);

        if (arguments.length >= 4 && extras)
            Object.assign(this, extras);

        //this.message = message;
        this.name = "ApiError";  // code = 200 でエラーがある。
        this.statusCode = code;
        this.statusText = toName(code);
        response && (this.response = response);
    }
}

export default ApiError;
