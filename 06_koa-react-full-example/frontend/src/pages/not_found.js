import React from "react";

function NotFound() {
    return (
  <div>
    <h1>404 <small>| Not Found</small></h1>

    <p>Sorry the path does not exist on this site.</p>
    <p>Are you sure you have the right url?</p>
  </div> );
}

export default NotFound;
