import React, {useState, useContext} from "react";
// React Router v6: withRouter は廃止.
import { useNavigate, Navigate } from 'react-router-dom';
import {useForm} from 'react-hook-form';
import {AuthStore, AuthUserContext, ProtectProvider, RoleType} from '../../components/auth_user_provider';


// フィールドをまたぐ検査は、この書き方がよい。
const validator = async (values, context) => {
    let errors = {};
    if ( !values.username.trim() ) {
        errors.username = {
            type: 'required',
            message: 'Email field must have at least 1 character.' };
    }

    if ( !values.password.trim() ) {
        errors.password = {
            type: 'required',
            message: 'Password field must have at least 1 character.' };
    }

    if ( values.password !== values.password_confirmation ) {
        errors.password_confirmation = {
            type: 'other',  // required, maxLength, minLength, max, min, pattern, ...
            message: "Do not match password confirmation" };
    }

    return {
        values: Object.keys(errors).length > 0 ? {} : values,
        errors: errors };
};


function SignUp() // extends React.Component
{
    // react-router v6: リダイレクトのやり方も変わった。
    const navigate = useNavigate();

    const { register, handleSubmit, setError, reset, formState:{errors} } =
          useForm({mode:'onChange',
                   resolver: validator,
                   defaultValues: {
                       username: '',
                       password: '',
                       password_confirmation: '',
                       admin_role: false }  });

    const [state, setState] = useState({
        //error: null,  // {message: string}
        //redirect: false,
    });

    const onSubmit = (formData) => { // handleSubmit = (ev) => {
        //ev.preventDefault();
        //console.log(this.myRefs); // `current` しか定義されない。これはアカン.
        const username = formData.username.trim();

        AuthStore.instance().register( {
            username: username,
            password: formData.password,
            password_confirmation: formData.password_confirmation,
            admin_role: formData.admin_role })
            .then( () => {
                navigate('/',
                         {state: {notice:"ユーザ登録しました!"}});
                // react-router の `withRouter` wrapper を使ってもよい;
                //   1. このメソッド内で,
                //      this.props.history.push('/thank-you');
                //   2. ファイル末尾で
                //      export default withRouter(SignUp);    // 囲む
            }).catch (err => {
                console.log('register() failed: ', err);
                setError('root', err.response.error);
            });

        // この書き方は, react-router v0.13.x 時代のもの。いつだよ?
        // はるか昔の Navigation mixin による.
        // this.context.router.replaceWith("index");
    };

    const currentUserId = useContext(AuthUserContext).currentUserId;
    if ( currentUserId ) {
        return <Navigate to="/" state={{alert:'Already logged in!'}} />;
    }

    const renderErrorBlock = () => {
        return errors.root ?
            (<div className="alert alert-danger" role="alert">{errors.root.message}</div>) :
            "";
    };

    return (<>
  <div>
    <h1>Sign Up</h1>
    {renderErrorBlock()}

    <form onSubmit={handleSubmit(onSubmit)}
          className={Object.keys(errors).length > 0 ? "has-error" : null} >
      <div className="row mb-3">
        <label htmlFor="inputEmail3" className="col-sm-2 col-form-label">Email:</label>
        <div className="col-sm-10">
          <input type="email" name="username" className="form-control" id="inputEmail3" {...register("username")} />
          {errors.username ? <div>{errors.username.message}</div> : ''}
        </div>
      </div>

      <div className="row mb-3">
        <label htmlFor="inputPassword3" className="col-sm-2 col-form-label">Password:</label>
        <div className="col-sm-10">
          <input type="password" name="password" className="form-control" id="inputPassword3" {...register("password")} />
          {errors.password ? <div>{errors.password.message}</div> : ''}
        </div>
      </div>

      <div className="row mb-3">
        <label htmlFor="password_confirmation" className="col-sm-2 col-form-label">Password confirmation:</label>
        <div className="col-sm-10">
          <input type="password" name="password_confirmation" className="form-control" id="password_confirmation" {...register('password_confirmation')} />
          {errors.password_confirmation ? <div>{errors.password_confirmation.message}</div> : ''}
        </div>
      </div>
      <div class="row mb-3">
        <label className="col-sm-2 col-form-label">Roles:</label>
        <div class="col-sm-10">
          <div class="form-check">
            <input className="form-check-input" type="checkbox" name="admin_role" id="gridCheck1" {...register('admin_role')} />
            <label className="form-check-label" htmlFor="gridCheck1">
              Admin role
            </label>
          </div>
        </div>
      </div>

      <button type="submit" className="btn btn-primary">Register</button>
    </form>
  </div>
                </> );
}


export default SignUp;
