
import React, {useState, useContext} from "react";
import { Link, useNavigate, useLocation } from "react-router-dom";
// React Router v6: withRouter は廃止.
//import { withRouter } from 'react-router';
import {useForm} from 'react-hook-form';
import {AuthStore, AuthUserContext, ProtectProvider, RoleType} from '../../components/auth_user_provider';


function SignIn() // extends React.Component
{
    // react-router v6: リダイレクトのやり方も変わった。
    const navigate = useNavigate();
    const location = useLocation();

    const { register, handleSubmit, setError, reset, formState:{errors} } =
          useForm({mode:'onChange',
                   //resolver: validator,
                   defaultValues: {
                       username: '',
                       password: '',
                       remember: '' } });

    const [state, setState] = useState({
        //error: props.initialError,
        //redirect: false,
    });

    const onSubmit = (formData) => {
        //ev.preventDefault();
        const username = formData.username.trim();

        AuthStore.instance().signIn({username:username,
                                     password: formData.password,
                                     remember: formData.remember })
            .then ( () => {
                // redirect back
                let to = location.state && location.state.from ? location.state.from : "/account";
                navigate(to, {state: {notice: 'Login Succeeded'}});
            }).catch (err => {
                setError('root', err.response.error);
            });
    }

    const renderErrorBlock = () => {
        return errors.root ? (
            <div className="alert alert-danger" role="alert">
    Bad login information: {errors.root.message}
  </div> ) : null;
    };

    const currentUserId = useContext(AuthUserContext).currentUserId;

    return (<>
  <div>
    { currentUserId ? <h1>別のユーザでサインイン</h1> :
    <h1>Sign In</h1> }

    <p>ダミーテキスト. ダミー. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
            dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex
            ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat
            nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit
            anim id est laborum. <a href="https://en.wikipedia.org/wiki/Lorem_ipsum">https://en.wikipedia.org/wiki/Lorem_ipsum</a></p>

    {renderErrorBlock()}

    <form onSubmit={handleSubmit(onSubmit)}
          className={Object.keys(errors).length > 0 ? "has-error" : null}>
      <div class="row mb-3">
        <label htmlFor="inputEmail3" className="col-sm-2 col-form-label">Email:</label>
        <div class="col-sm-10">
          <input type="email" name="username" className="form-control" id="inputEmail3" {...register("username", {required:true})} />
          {errors.username && <div>This field is required</div>}
        </div>
      </div>

      <div class="row mb-3">
        <label htmlFor="inputPassword3" className="col-sm-2 col-form-label">Password:</label>
        <div class="col-sm-10">
          <input type="password" name="password" className="form-control" id="inputPassword3" {...register("password", {required:true})} />
          {errors.password && <div>This field is required</div>}
        </div>
      </div>

      <div class="row mb-3">
        <div class="col-sm-10 offset-sm-2">
          <div class="form-check">
            <input className="form-check-input" type="checkbox" name="remember" id="gridCheck1" {...register('remember')} />
            <label className="form-check-label" htmlFor="gridCheck1">
              Example checkbox
            </label>
          </div>
        </div>
      </div>

      <button type="submit" className="btn btn-primary">Sign in</button>
    </form>

    <p>Don't have an account? You can <Link to="/account/sign_up">Sign up</Link>.</p>
  </div>
  </> );
}


// this.props.location を使うときは withRouter() で囲む.
export default SignIn;
