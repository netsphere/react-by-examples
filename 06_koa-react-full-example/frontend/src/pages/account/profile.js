
// React Hook "useContext" cannot be called in a class component. React Hooks
// must be called in a React function component or a custom React Hook function
import React, {useContext} from "react";
import {AuthStore, AuthUserContext, ProtectProvider, RoleType} from '../../components/auth_user_provider';

function ProfilePage() // extends React.Component
{
    const currentUserId = useContext(AuthUserContext).currentUserId;
    const user = currentUserId ? AuthStore.getUser(currentUserId) : {};

    return (<>
  <h1>Profile</h1>

  <table class="table">
    <tbody>
      <tr><th>email:</th>      <td>{user.email}</td></tr>
      <tr><th>admin_role:</th> <td>{user.admin_role}</td></tr>
      <tr><th>password:</th>   <td>{user.password}</td></tr>
      <tr><th>created_at:</th> <td>{user.created_at}</td></tr>
      <tr><th>updated_at:</th> <td>{user.updated_at}</td></tr>
    </tbody>
  </table>
            </> );
}


export default ProfilePage;
