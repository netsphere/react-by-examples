import React from "react";
import Counter from "../components/counter";

function IndexPage(props) {
    return (
  <div>
    <h1>Index - Super Counter</h1>

    <Counter />

    <p><a href="/account">Profile</a></p>
  </div> );
}

export default IndexPage;
