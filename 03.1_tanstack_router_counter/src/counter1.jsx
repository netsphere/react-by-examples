"use client";

// 参考 https://zenn.dev/hayato94087/articles/8976bc31b9e3e6
//      React で Counter を作成

import { useState } from 'react';

export default function Counter1 () {
  // `setCount()` は非同期関数. 値の更新は関数呼び出し時点ではない。
  const [count, setCount] = useState(0);

  const increment = () => {
      // setCount(count + 1);  誤った実装
      // 値ではなく 1引数の関数を渡すと, その時点での値を参照できる。
      setCount((prevCount) => prevCount + 1);
  };
  const decrement = () => {
      // setCount(count - 1);  誤った実装
      setCount((prevCount) => prevCount - 1);
  };
  const increment2 = () => {
      increment();
      increment();
  };
  const decrement2 = () => {
      decrement();
      decrement();
  };

  return <div>
    <span>count = {count}</span>
    <div>
      <button onClick={decrement2}>2 減らす</button>
      <button onClick={decrement}>1 減らす</button>
      <button onClick={increment}>1 増やす</button>
      <button onClick={increment2}>2 増やす</button>
    </div>
  </div> ;
}
