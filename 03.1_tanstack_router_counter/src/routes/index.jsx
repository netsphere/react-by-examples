import * as React from 'react'
import { createFileRoute } from '@tanstack/react-router'
import Counter1 from '../counter1';

export const Route = createFileRoute('/')({
  component: HomeComponent,
})

function HomeComponent() {
  return (
    <div className="p-2">
      <h1>Welcome Home!</h1>
      <Counter1 />
    </div>
  )
}
