
async function my_sleep(code, delay_ms) {
    return new Promise(resolve => {
        setTimeout(() => {resolve(code)}, delay_ms);
    });
}

class ArticleModel
{
    static posts = [
        {id: 1, title: '1 This is テ<big>スト',
         body: 'テスト\nテ<big><big>ストふがふが'},
        {id: 2, title: '2 そ<big>の2',
         body: 'hogehoge\n\nho<big><big>gehoge'},
        {id: 3, title: '3 そ<big><big>の3a',
         body: 'WWWWWWWWWWW\n\nho<big><big>gehoge'},
    ]

    static nextId = 3;

    static find_all(filter) {
        return ArticleModel.posts;
    }

    static async find(filter) {
        const ary = ArticleModel.posts.filter( (t) => t.id === Number(filter) );
        if (ary.length === 0)
            throw new Error('Record not found');
        return my_sleep(ary[0], 3000);
    }

    static create(item) {
        const article = {
            id:    (Number(item.id) > 0 ? item.id : ArticleModel.nextId++),
            title: item.title.trim(),
            body:  item.body };
        if (article.title === 'NG')
            throw new Error('NG word!!');  // エラーメッセージ表示のために

        ArticleModel.posts = [
            ...(ArticleModel.posts.filter(t => t.id !== article.id)),
            article];
        return article;
    }

    static delete(filter) {
        const id = Number(filter);
        if ( !(id > 0) )
            throw new RangeError(":id must be aNumber");
        ArticleModel.posts = [
            ...(ArticleModel.posts.filter(t => t.id !== id))];
        return true;
    }
}

export default ArticleModel;
