
import React, {useState, useEffect} from 'react';
import ArticleModel from '../../ArticleModel';
// <Redirect> は react-router v6 で単に廃止。<Navigate> に置き換え.
import { Link, useParams, useNavigate } from 'react-router-dom';


// 編集ページ
function ArticleEdit() // extends React.Component
{
    // react-router v6: URL 引数の受け取り方が変わった。ヒドい。
    const params = useParams();
    // react-router v6: リダイレクトのやり方も変わった。
    const navigate = useNavigate();

    let { id } = params;

    const [state, setState] = useState({
        isLoaded: Number(id) > 0 ? false : true,
        error: null,
        submitEnabled: false,

        // 'Post' fields
        id: null,
        title: '',
        body: ''  });

    if (Number(id) > 0 && !state.isLoaded) {
        // Edit
        ArticleModel.find(id).then( post => {
            setState({...state,
                      isLoaded:true, error:null,
                      id: post.id, title: post.title, body: post.body}); });
    }
/*
    else {
        // New
        setState({...state,
                  isLoaded:true });
    } */

    useEffect( () => { // componentDidMount() {
        document.title = `Article Edit`;
    });

    const handleSubmit = (event) => {
        event.preventDefault();
        const title = state.title.trim();
        if (!title) {
            return setState({...state,
                             error:{message:'Title field must have at least 1 character.'},
                             submitEnabled: false});
        }

        try {
            const article = ArticleModel.create({
                              id:state.id, title:title, body:state.body});
            // Succeeded
            navigate(`/articles/${article.id}`,
                     {state:{flash:`Item "${title}" updated.`}});
        }
        catch (err) {
            setState({...state, error:err} );
        }
    }

    // リアルタイム更新
    const onFieldChanged = async (event) => {
        const name = event.target.name;
        // checkbox の場合は, 場合分け必要. event.target.checked
        const value = event.target.value;
        setState( {...state,
                   submitEnabled: (name === 'title' ? value : state.title).trim() !== '',
                   [name]:value });
    };

    return (<>
                { !state.isLoaded ? (
  <div>Loading...</div>
                ) : (
  <div>
  {state.error ? <div class="alert alert-danger" role="alert">
                        Error: {state.error.message}</div> : ''}
    <form onSubmit={handleSubmit} >
      <div class="form-group row">
        <label for="inputTitle" className="col-sm-2 col-form-label">Title:</label>
        <div class="col-sm-10">
          <input id="inputTitle" name="title" type="text" class="form-control"
                 value={state.title}
                 onChange={e => onFieldChanged(e)} />
        </div>
      </div>
      <div class="form-group row">
        <label className="col-sm-2 col-form-label">Body:</label>
        <div class="col-sm-10">
          <textarea name="body" class="form-control" value={state.body}
                    onChange={e => onFieldChanged(e)} />
        </div>
      </div>
      <Link to={state.id ? `/articles/${state.id}` : `/articles/`}
            replace className="btn btn-secondary">Cancel</Link>
      <button type="submit" class="btn btn-primary"
              disabled={state.submitEnabled ? '' : 'disabled'}>
        {Number(state.id) > 0 ? 'Update' : 'Create'}</button>
    </form>
  </div> )}
  </>);
}


export default ArticleEdit;
