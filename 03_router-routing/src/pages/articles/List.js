
import React, {useState} from 'react';
import ArticleModel from '../../ArticleModel';
// react-router v6: location の取り出し方が変わった
// React Hook "useLocation" cannot be called in a class component. React Hooks
// must be called in a React function component or a custom React Hook function.
import { Link, useLocation } from 'react-router-dom';


function ArticleList() // extends React.Component
{
    // react-router v6: props.location が来てない。ヒドい。
    const location = useLocation();

    const [state, setState] = useState({
        flash: location.state ? location.state.flash : null,
        posts: ArticleModel.find_all(),
    });

    return <>
  <h1>Article List</h1>
  {state.flash ? <div class="alert alert-success">{state.flash}</div> : ''}
  <p><Link to="/articles/new">New article...</Link></p>
  <table class="table">
    <thead>
      <tr><th>Title</th></tr>
    </thead>
    <tbody>
            {state.posts.map(post => (
      <tr>
        <td>{post.title}</td>
        <td><Link to={`/articles/${post.id}`}>Show</Link></td>
      </tr>
            ))}
    </tbody>
  </table>
</> ;
}


export default ArticleList;
