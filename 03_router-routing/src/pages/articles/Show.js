
import React, { useState, useEffect } from 'react';
import ArticleModel from '../../ArticleModel'
// - <Redirect> は react-router v6 で単に廃止。<Navigate> に置き換え.
// - React Hook "useParams" cannot be called in a class component. React Hooks
//   must be called in a React function component or a custom React Hook function.
import { Link, useParams, useNavigate, useLocation } from 'react-router-dom';


// 改行を <br /> に変換
function nl2br(text) {
    // gフラグ: Global search.
    const re = /(\r?\n)/g
    return text.split(re).map(function(line) {
            if (line.match(re))
                return React.createElement('br');
            else
                return line;
    });
}


function ArticleShow() // extends React.Component
{
    // react-router v6: URL 引数の受け取り方が変わった。ヒドい。
    const params = useParams();
    // react-router v6: リダイレクトのやり方も変わった。
    const navigate = useNavigate();
    const location = useLocation();

    //const [redirectToList, set_redirectToList] = useState(false);
    const [state, setState] = useState({
        flash: location.state ? location.state.flash : null,
        //isLoaded: false,
        error: null,
        post: null });
    ArticleModel.find(params.id).then( post => {
        setState({...state,
                  error:null, post:post}); });

    useEffect( () => { //componentDidMount() {
        document.title = state.post ? state.post.title : 'Loading...';
    });

    const onDelete = () => {
        //const post = state.post;
        if (window.confirm(`Are you sure you want to delete "${state.post.title}"?`)) {
            if (ArticleModel.delete(state.post.id) ) {
                // redirect
                navigate('/articles/',
                         {state:{flash:`Item "${state.post.title}" removed.`}});
            }
        }
    }

    return (<>
  {state.error ? <div class="alert alert-danger" role="alert">
                               Error: {state.error.message}</div> : ''}
                { !state.post ? (
  <div>Loading...</div>
                ) : (
  <article>
    <h1>{state.post.title}</h1>
    {state.flash ? <div class="alert alert-success">{state.flash}</div> : ''}
    <div>
                { nl2br(state.post.body) }
    </div>
    <Link to={`/articles/${state.post.id}/edit`} className="btn btn-secondary">Edit...</Link>
    <button type="button" onClick={onDelete} className="btn btn-secondary">Delete</button>
  </article> )}
  </>);
}

export default ArticleShow;
