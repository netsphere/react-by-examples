
import React from 'react';
import ArticleModel from '../ArticleModel';
import { Link } from 'react-router-dom';


class Home extends React.Component
{
    constructor(props) {
        super(props);
        this.state = {
            posts: ArticleModel.find_all(),
        }
    }
    
    render() {
        return <div>
  <h1>Home</h1>
  {this.state.posts.slice(-1).map(post => (
                 <div>{post.title}
                   <Link to={`/articles/${post.id}`}>Show</Link>  
                 </div>      
               ))}                           
  <Link to={`/articles/`}>List</Link>
</div> ;
    }
}

export default Home;
