
# Next.js サンプル

This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/blob/canary/packages/create-next-app/).


## パッケージ選定

 - <s>bookshelf.js</s> -- a JavaScript ORM for Node.js, built on the <i>Knex</i> SQL query builder. 列の取り出しは `model.get(column)`.
   [2023-04] 最終リリースから3年が経過しており、開発終了か? -- knex とヴァージョン不整合。もう使えない。<i>Objection.js</i> に変更。
   ```
   peer knex@">=0.15.0 <0.22.0" from bookshelf@1.2.0
   ```

 - knex.js -- A SQL query builder that is flexible, portable, and fun to use!
   対応アダプタ: `better-sqlite3` or `sqlite3` (両対応), `pg` or `pg-native` (両対応), `mysql` or `mysql2`. Oracle (`oracledb` パッケージ) も対応.

 - Next.js

 - <i>React</i>    `react-dom` for the web, or `react-native` for the native environments.

 - node-sqlite3 ("sqlite3" package) -- Asynchronous, non-blocking SQLite3 bindings for Node.js. You can use "better-sqlite3" package also. 



## How to run

`next` コマンドが使えることを確認.

```shell
  $ npx next -h
```

First, run the development server:

`knexfile.js.sample` を `knexfile.js` にコピーして、適宜編集.

```shell
  $ npm install
  $ npx knex migrate:latest
  $ npm run dev
```
or
```shell
  $ yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.


### 本番環境用にビルド

```
  $ npm run build
  $ npm start
```



## Topic: knex.js

Migration の作成. `/migrations/20210831135944_make_article.js` ファイルが生成される。

<pre>
$ <kbd>npx knex migrate:make make_article</kbd>
</pre>



