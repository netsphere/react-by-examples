// Config file: `next.config.js` or `tsconfig.json`

// webpack の設定もこのファイルでおこなう。

module.exports = {
    reactStrictMode: true,

    webpack: (config, { isServer }) => {
        if (!isServer) {
            // don't resolve 'fs' module on the client to prevent this error on build --> Error: Can't resolve 'fs'
            // See https://jasonwatmore.com/post/2021/07/30/next-js-webpack-fix-for-modulenotfounderror-module-not-found-error-cant-resolve
            config.resolve.fallback = {
                fs: false
            }
        }

        return config;
    }
}
