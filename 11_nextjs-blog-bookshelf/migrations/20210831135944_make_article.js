"use strict";

// @note function() 内で return するのを忘れないように!
//       Web上のサンプルだと return していないものも見える.
exports.up = function(knex) {
    return knex.schema.raw(
        'CREATE TABLE articles(' +
        '  id         INTEGER PRIMARY KEY AUTOINCREMENT, ' +
        '  title      VARCHAR(200) NOT NULL, ' +
        '  body       TEXT NOT NULL, ' +
        '  created_at TIMESTAMP NOT NULL, ' +
        '  updated_at TIMESTAMP NOT NULL)');
};

exports.down = function(knex) {
    return knex.schema.dropTable('articles');
};
