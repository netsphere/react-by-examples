
// Server-side.
// PUT /posts/:id  => update
// DELETE /posts/:id  => destroy

import Article from '../../../models/article';

// @param req  the request Object.
//     url: '/api/articles/11'
//     query: { id:'11' },      URLパラメータはこちら.
//     body: {},
const ArticleUpdateOrDestroy = async (req, res) => {
    let article = await Article.query().findById(req.query.id);
    if (!article) {
        res.statusCode = 404;
        return res.json({
            error: {
                code: "404",
                message: "Record Not Found",
            }
        });
    }

    if (req.method === 'PUT' || req.method === 'PATCH') {
        // 更新
        // await article.update(req.body); // これは Knex のメソッド. => アカン
        try {
            await Article.query().update({
                title: req.body.title,
                body:  req.body.body
            }).where('id', article.id);
            return res.json(article);
        }
        catch (err) {
            res.statusCode = 400;
            return res.json({
                error: {
                    code: "400",
                    message: "Article#save() validation error",
                    "details": [{
                        "message": err.message,
                    }]
                }
            });
        }
    }
    else if (req.method === 'DELETE') {
        // 削除
        await Article.query().delete().where('id', article.id);
        return res.json(true);
    }
    else {
        res.statusCode = 405; // Method Not Allowed
        return res.json({
            error: {
                code: "405",
                message: "Method Not Allowed!!"
            }
        });
    }
}

export default ArticleUpdateOrDestroy;
