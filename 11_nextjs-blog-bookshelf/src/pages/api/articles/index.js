
// Server-side.
// POST /posts => create

// 投稿の作成実行.

const bookshelf = require('../../../connection');
import Article from '../../../models/article';

const ArticleCreate = async (req, res) => {
    if (req.method !== 'POST') {
        res.statusCode = 405 // Method Not Allowed
        res.json({
            error: {
                code: "405",
                message: "Method Not Allowed!!"
            }
        });
        return;
    }

    //console.log(req.body); //=> { title: 'sadfsdf', body: 'sadfasdf' }

    let article = {
        title: req.body.title,
        body:  req.body.body
    };
    //article.title = res.body.title;
    //article.body = res.body.body;
    try {
        article = await Article.query().insert(article);
        console.log(article.attributes); // id フィールドが更新される.
        return res.json(article);
    }
    catch (err) {
        res.statusCode = 400;
        return res.json({
            error: {
                code: "400",
                message: "Article#save() validation error",
                "details": [{
                    "message": err.message,
                }]
            }
        });
    }
}

export default ArticleCreate;
