
// a Global Stylesheet
import '../styles/globals.css'
import Head from 'next/head'; // 'next/document' ではない.
// デフォルトの <Layout>
// Per-page layout については, https://nextjs.org/docs/basic-features/layouts
import Layout from '../components/layout';

// Web上の解説で, ここに共通 <Head>..</Head> を仕込むサンプルがあるが,
// ここではなく, _document.js に書け.
// => ただし, `<meta name="viewport" ...>` はここに書く.
function MyApp({ Component, pageProps }) {
    return (<Layout>
  <Head>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
  </Head>
  <Component {...pageProps} />
  </Layout> );
}

export default MyApp
