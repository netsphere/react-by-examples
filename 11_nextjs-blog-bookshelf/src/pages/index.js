// 'use strict'; // Next.js では書かなくてもよい.

// <Head> component. HTML head タグ内に追記できる.
import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'
import Link from 'next/link'

/*
Server Error
Error: Invalid <Link> with <a> child. Please remove <a> or use <Link legacyBehavior>.
Learn more: https://nextjs.org/docs/messages/invalid-new-link-with-extra-anchor
  -> Next.js 13 から書き方が変わった。
*/

// pages/ 以下に置くことで、自動的に path と対応づけられる.
// 関数名はファイル名と一致していなくてもよい.
export default function Home() {
    // <Link> はブラウザの history を更新する.
    return (
      <div className={styles.container}>
        <Head>
          <title>index page - Create Next App</title>
        </Head>

        <div className="main">
          <h1 className={styles.title}>Blog sample</h1>

          Read <Link href="/posts/first-post">this page!</Link>

          <p><Link href="/posts/">投稿一覧</Link></p>
        </div>

          <footer className={styles.footer}>
            <a
              href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
              target="_blank"
              rel="noopener noreferrer"
            >
              Powered by{' '}
              <span className={styles.logo}>
                <Image src="/vercel.svg" alt="Vercel Logo" width={72} height={16} />
              </span>
            </a>
          </footer>
      </div>
    );
}
