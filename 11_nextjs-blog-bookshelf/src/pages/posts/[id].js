
// クライアント側
// ファイル名を `[id].js` にすると, :id に値が入る. 数値とは限らない.

// 関数コンポーネントのときは useRouter を使う.
import { useRouter } from 'next/router'
import Link from 'next/link';

//import Layout from '../../components/layout'

// Next.js v9.3: getInitialProps は非推奨.
// Use `getStaticProps` or `getServerSideProps` instead of `getInitialProps`.
//
// `getStaticProps`:     ビルド時 or 必要になったときに呼び出される. サーバ側で
//                       実行される. 爾後のリクエストに対しては、静的な結果が使
//                       われる.
// `getServerSideProps`: Mount 時に呼び出される。サーバ側で実行される.
//
// これらの中で必要なデータ取得などを行い, hash を返せばよい.
// => Renderer で利用できる。

import ArticleAPI from '../../stores/article';

// getStaticProps() を使うときは getStaticPaths() が必須. (ないとエラー)
// This function gets called at build time
export async function getStaticPaths() {
    return {
        paths: [
            //{ params: {...} }
        ],
        fallback: 'blocking',  // `fallback` key 必須.
                           // true にすると, "Loading..." などを表示しつつ描画.
                           // 'blocking' は, 描画まで待つ.
    };
}


// 初回のアクセスのときに、データを取得する。
// @param context 次のようなオブジェクト;
//      {
//        params: { id: '2' },
//        locales: undefined,
//        locale: undefined,
//        defaultLocale: undefined
//      }
export async function getStaticProps( context )
{
    // Reloadしても, revalidate の秒数が経過するまで, 再呼び出しされない.
    console.log("Here is [id].js getStaticProps()");

    const Article = require('../../models/article');

    // 動的パラメータを取り出す
    const { id } = context.params;
    let post;
    try {
        post = await Article.query().findById(id); //.fetch();
    } catch (err) {
        post = null;
    }

    return {
        notFound: !post,
        props: {
            //post: post,
            post: JSON.parse(JSON.stringify(post)) // workaround
        },
        revalidate: 10, // In seconds.
    };
}


// Render する. こちらは reload の度に走る.
// @param data  `getStaticProps` or `getServerSideProps` の戻り値の, `props`.
export default function PostsShow( data ) {
    const router = useRouter();
    const post = data.post;

    const onDelete = async () => {
        if (window.confirm(`Are you sure you want to delete "${post.title}"?`)) {
            await ArticleAPI.delete(post.id);
            router.push(`/posts/`);
        }
    };

    return (
  <div>
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/">Home</a></li>
        <li class="breadcrumb-item"><a href="/posts/">Posts</a></li>
      </ol>
    </nav>
    <h1>Post: {post.title}</h1>
          <div>
            Body: {post.body}
          </div>
          <Link href={`/posts/${encodeURIComponent(post.id)}/edit`}>
            Edit...
          </Link>
          <button type="button" onClick={onDelete}>Delete</button>

          <p>Generated at {(new Date()).toString()}</p>
        </div>
    );
}
