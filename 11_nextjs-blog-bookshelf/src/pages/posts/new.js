
// クライアント側
// 新規作成フォーム

import React from 'react'
// Class コンポーネントのときは withRouter を使う.
import { withRouter } from 'next/router';
import Link from 'next/link';

import ArticleAPI from '../../stores/article';
import Form from './_form';


class PostsNew extends React.Component {
    constructor(props) {
        super(props)

        this.post = {
            id: null,
            title: '',
            body: '' };

        this.state = {
            errors: null
        };
    }

    // ここはクライアント側で動く
    handleSubmit = async event => {
        event.preventDefault()

        ArticleAPI.create(this.post)
            .then( article => {
                // Redirect.
                this.props.router.push(`/posts/${article.id}`);
            }).catch( err => {
                console.log(err);
                this.setState({errors: {
                    message:err.message } });
            });
    };

    updateState = (state) => {
        for (const prop in state)
            this.post[prop] = state[prop];
    }

    render() {
        return (
  <div>
    {this.state.errors ? JSON.stringify(this.state.errors) : ''}
    <Form post={this.post} handleSubmit={this.handleSubmit}
          updateState={this.updateState} />
  </div> );
    }
}

export default withRouter(PostsNew);
