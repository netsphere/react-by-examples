
import Link from 'next/link';
import Head from 'next/head';

// import 文なら top level に書いても OK.
import Article from '../../models/article';

// サーバ側で動く!
// @param context オブジェクト.
//     params: Dynamic route の場合の引数.
//     req: リクエスト
//     res: ServerResponse オブジェクト.
export async function getServerSideProps( context )
{
    // サーバで動かすモジュールは, getServerSideProps() 内から require() しなけ
    // ればならない。
    // 上記のとおり, import 文なら top level に書いてよい.
    const bookshelf = require('../../connection');

    // 素の Bookshelf.js には findAll() や findOne() のようなメソッドはない.
    let posts = await Article.query().orderBy('id');

    // props: renderer に渡す hash を設定する.
    // notFound: true にすると, 404 status になる。
    return {
        props: {
            // すごい鈍くさい.
            // See https://github.com/vercel/next.js/issues/11993
            //posts: posts,
            posts: JSON.parse(JSON.stringify(posts)) // workaround
        }
    };
}


// Renderer: クライアント側で実行される.
export default function PostsList( data ) {
    const posts = data.posts;

    return <>
  <Head>
    <title>投稿一覧</title>
  </Head>
  <div>
    <ul>
    {posts.map((post) => (
      <li key={post.id}>
        <Link href={`posts/${encodeURIComponent(post.id)}`}>
          {post.title}
        </Link>
      </li>
    ))}
    </ul>
    <Link href="/posts/new">
      New...
    </Link>
  </div>
            </>;
}
