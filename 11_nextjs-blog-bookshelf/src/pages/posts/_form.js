
import React from 'react';
import Link from 'next/link';

class Form extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            title: props.post ? props.post.title : '',
            body:  props.post ? props.post.body : '' };
    }

    // TODO:
    validate() {
        console.log("validate(): ", this.state);
    }

    onFieldChanged = (event) => {
        const target = event.target;
        const name = target.name;
        const value = target.type === 'checkbox' ? target.checked : target.value;

        // setState() は非同期メソッド。確実に state の変更後にアクションを行う
        // には, callback [第2引数] として書く.
        this.props.updateState({[name]:value});
        this.setState( {[name]:value}, this.validate );
    }

    render() {
        const {id: post_id} = this.props.post || {};

        return (
          <div>
            <form onSubmit={this.props.handleSubmit} >
              <div className="row mb-3">
                <label htmlFor="post_title" className="col-sm-2 col-form-label">Title:</label>
                <div className="col-sm-10">
                  <input id="post_title" name="title" type="text" value={this.state.title}
                         required className="form-control" onChange={this.onFieldChanged} />
                </div>
              </div>
              <textarea name="body" rows="10" value={this.state.body} onChange={this.onFieldChanged} />
              <Link href={post_id > 0 ? `/posts/${post_id}` : `/posts/`}>
                キャンセル</Link>
              <button type="submit" className="btn btn-primary" >
                {post_id > 0 ? "Update" : "Create"}</button>
            </form>
          </div>
        );
    }
}

export default Form;
