
// クライアント側
// /posts/:id/edit

// 編集フォーム. 基本 new といっしょ。=> TODO: コードも統合できない?

import React from 'react'
// Class コンポーネントのときは withRouter を使う.
import { withRouter } from 'next/router';

import ArticleAPI from '../../../stores/article';
import Form from '../_form';


// サーバ側
export async function getServerSideProps( context )
{
    const Article = require('../../../models/article');

    // Class-based コンポーネントから使えるかな?
    // 動的パラメータを取り出す
    const { id } = context.params;
    let post;
    try {
        post = await Article.query().findById(id); //.fetch();
    } catch (err) {
        post = null;
    }

    return {
        notFound: !post,
        props: {
            post: JSON.parse(JSON.stringify(post)) // workaround
        },
    };
}


class PostsEdit extends React.Component {
    constructor(props) {
        super(props)

        this.post = props.post;
        this.state = {
            errors: null,
        };
    }

    // ここはクライアント側で動く
    handleSubmit = async event => {
        event.preventDefault()

        ArticleAPI.create(this.post)
            .then( article => {
                // Redirect.
                this.props.router.push(`/posts/${article.id}`);
            })
            .catch( err => {
                console.log(err);
                this.setState({errors: {
                    message:err.message,
                    //detail_message:err.cause.details[0].message
                }});
            });
    };

    updateState = (state) => {
        for (const prop in state)
            this.post[prop] = state[prop];
    };

    render() {
        return (
  <div>
    {this.state.errors ? JSON.stringify(this.state.errors) : ''}
    <Form post={this.post} handleSubmit={this.handleSubmit}
          updateState={this.updateState} />
  </div> );
    }
}

export default withRouter(PostsEdit);
