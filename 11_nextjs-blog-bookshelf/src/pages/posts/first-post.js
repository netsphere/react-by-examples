
import Link from 'next/link';

export default function FirstPost() {
    // <Link> は, client-side navigation になる.
    return <>
      <h1>First Post</h1>
      <h2>
        <Link href="/">
          Back to home
        </Link>
      </h2>
    </>;            
}
