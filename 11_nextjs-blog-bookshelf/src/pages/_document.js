
// See https://nextjs.org/docs/advanced-features/custom-document

import { Html, Head, Main, NextScript } from 'next/document'

// <body> は小文字.
// `<meta name="viewport" ...>` は, ここではなく, `_app.js` に書け. Why?
export default function Document() {
    return (
<Html>
  <Head>
    <link href="/bootstrap5/css/bootstrap.min.css" rel="stylesheet" />
    <link rel="icon" href="/favicon.ico" />
  </Head>
  <body>
    <script src="/bootstrap5/js/bootstrap.bundle.min.js"></script>
    <Main />
    <NextScript />
  </body>
</Html>
    );
}
