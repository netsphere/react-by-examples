"use strict";

const Model = require('../connection');
const { Validator, ValidationError } = require('objection');
//const Comment = require('./Comment');

/*
Objection.js: バリデーションは, 次の方法がある:
  1. JSON Schema で指定する
  2. $beforeInsert() と $beforeUpdate() に書く
  3. カスタムバリデータを作る
上記 1. は DRY 原則に反するため、2. か 3. がよい。
*/

class MyValidator extends Validator
{
    // Must override
    validate(args) {
        const data = args.json;

        // エラーはキーごとの配列になる。
        const errors = {};
        let title = (data.title || "").normalize("NFKC").trim();
        if (title === "") {
            errors.title = (errors.title || []).push({
                message: "`title` is required.",
                keyword: 'required' });
        }
        if (title === "NG") {
            errors.title = (errors.title || []).push({
                message: "`title` must not be 'NG'", });
        }
        data.title = title; // Objection がモデルにマージする。

        if (Object.keys(errors).length > 0) {
            throw new ValidationError({
                type: 'ModelValidation',
                message: 'validation error',
                data: errors });
        }

        return data; // これが必須
    }
}

class Article extends Model // bookshelf.Model.extend({
{
    static tableName = 'articles';

    // Timestamps
    $beforeInsert() {
        const now = new Date().toISOString();
        this.created_at = now; this.updated_at = now;
    }
    $beforeUpdate() { this.updated_at = new Date().toISOString(); }

    // override
    static createValidator() {
        return new MyValidator();
    }
}

module.exports = Article; // bookshelf.model('Article', Article);
