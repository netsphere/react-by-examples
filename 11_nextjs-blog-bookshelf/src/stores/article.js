
// Client-side.

import API from './api';


class Article extends API
{
    // Next.js: 本番環境では名前が変わるので、明示的に指定しなければならない。
    // override
    static resourceName = 'articles';


    static async find_latest() {
        //const model_name = this.name.toLowerCase() + "s";
        const response = await API.apicall('GET',
                                           "/" + this.resourceName + `/latest`);
        return API._body_or_error(response, "find_latest() error");
    }
} // class Article

export default Article;
