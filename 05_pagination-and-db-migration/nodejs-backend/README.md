
# Example for Express API server with Sequelize

## How to run

1. 依存パッケージのインストール

```shell
  $ npm install
```

2. データベース設定
  `config/database.js` ファイル

* [dev] データベースファイルを作る。プロジェクトディレクトリにて,
```shell
  $ npx sequelize-cli db:create
```
      => sqlite の場合は、これは不要。


テーブルを追加する

  $ npx sequelize-cli model:generate --name Tag --attributes name:string
     => src/models/tag.js
        migrations/2020....-Tag.js

   モデルのファイル名は小文字で生成される。




3. マイグレーション実行

<pre>
$ <kbd>npx sequelize-cli db:migrate</kbd>
Sequelize CLI [Node: 18.15.0, CLI: 6.6.0, ORM: 6.31.0]

Loaded configuration file "<code>config/database.js</code>".
Using environment "<code>development</code>".
== 20200328094506-create-article: migrating =======
== 20200328094506-create-article: migrated (0.048s)

== 20200405131828-create-tag: migrating =======
== 20200405131828-create-tag: migrated (0.011s)
</pre>



4. 開始

```shell
  $ npm start
```


Webブラウザでアクセス
  http://localhost:3001/api/articles




## Sequelize v6 でトランザクション

<i>Sequelize</i> は自動でトランザクションを有効にしてくれない。明示的にトランザクションオブジェクトを生成して、それをすべてのメソッドに引き回す必要がある。相当ダサい。

ロールバックも、例外を catch して明示的に行う必要がある。そのため、Sequelize のメソッドは漏れなくすべて await しなければならない。同期しないといけないメソッドがいちいち `Promise` を返す設計がおかしい。

```javascript
    const tr = await models.sequelize.transaction();
    try {
        await models.ArticlesTag.destroy({ where: {articleId:article.id},
                                            transaction: tr  });
        const tags = await update_tags(request.body.tags, tr);

        article.title = request.body.title;
        article.body = request.body.body;
        await article.save({transaction:tr});
        await update_articles_tags( article, tags, {transaction:tr} );
    }
    catch (err) {
        await tr.rollback();
        response.status(400).json({
                "error": {
                    "code": "400", // Bad Request
                    "message": err.message }
        });
        return;
    }
    await tr.commit();
    response.json( article );
```


