'use strict';

// npx sequelize-cli db:migrate

module.exports = {
    up: (queryInterface, Sequelize) => {
        // テーブル名は複数形.
        queryInterface.createTable('Tags', {
            id: {
                autoIncrement: true,
                primaryKey:    true,
                type:          Sequelize.INTEGER  },
            name: {
                type:      Sequelize.STRING,
                allowNull: false,
                unique:    true, },
            created_at: {
                allowNull: false,
                type: Sequelize.DATE },
            updated_at: {
                allowNull: false,
                type: Sequelize.DATE }
        });

        // 多対多でつなぐ.
        // createTable(tableName, attributes, options, model)
        return queryInterface.createTable('Articles_Tags', {
            id: {
                autoIncrement: true,
                primaryKey:    true,
                type:          Sequelize.INTEGER  },
            article_id: {  // モデル上は camelCase.
                type:      Sequelize.INTEGER,
                allowNull: false,
                references: {
                    model: "Articles",   // ここはテーブル名なので複数形
                    key:   "id" } },
            tag_id: {
                type:      Sequelize.INTEGER,
                allowNull: false,
                references: { model: "Tags", key:   "id" } }
        }, {
            uniqueKeys: {
                'Articles_Tags_u1': {
                    fields: ['article_id', 'tag_id']
                }}
        });
    },
    down: (queryInterface, Sequelize) => {
        queryInterface.dropTable('Articles_Tags');
        return queryInterface.dropTable('Tags');
    }
};
