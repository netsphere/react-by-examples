
// .sequelizerc がなくても, config/config.js ファイルを参照してくれる
// 環境変数 NODE_ENV で選択する

module.exports = {
    "development": {
        "dialect": "sqlite",
        "storage": "./db.dev.sqlite"
    },
    "test": {
        "username": "root",
        "password": null,
        "database": "database_test",
        "host": "127.0.0.1",
        "dialect": "mysql",
        "operatorsAliases": false
    },
    "production": {
        "username": "root",
        "password": null,
        "database": "database_production",
        "host": "127.0.0.1",
        "dialect": "mysql",
        "operatorsAliases": false
    }
};
