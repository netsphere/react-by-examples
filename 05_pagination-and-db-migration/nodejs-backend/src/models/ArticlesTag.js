'use strict';

module.exports = (sequelize, DataTypes) => {
    const ArticlesTag = sequelize.define('ArticlesTag', {
        id: {
            type:          DataTypes.INTEGER,
            primaryKey:    true,
            autoIncrement: true },
        articleId: { // camelCase.
            type:       DataTypes.INTEGER,
            allowNull:  false,
            references: {
                model: "Articles",   // ここはテーブル名なので複数形
                key:   "id" } },
        tagId: {
            type:      DataTypes.INTEGER,
            allowNull: false,
            references: { model: "Tags", key:   "id" } }
    }, {
        underscored: true,
        // 自動変換で対応できる。不要のはず
        //tableName: 'articles_tags',  // ここはテーブル名なので複数形.
        timestamps: false
    });

    ArticlesTag.associate = function(models) {
        ArticlesTag.belongsTo(models.Article);
        ArticlesTag.belongsTo(models.Tag);
    };
    
    return ArticlesTag;
};

