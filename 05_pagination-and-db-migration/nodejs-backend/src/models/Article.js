'use strict';

// 多対多リレーションシップのサンプル
module.exports = (sequelize, DataTypes) => {
    // モデル名は単数形
    const Article = sequelize.define('Article', {
        id:    {
            type: DataTypes.INTEGER,
            primaryKey: true,  // PK のときは allowNull:false ではない.
            autoIncrement: true     },
        title: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                notEmpty: true,  // don't allow empty strings

                // Custom validator
                myCustom(value) {
                    if (value === 'NG')
                        throw new Error('NG word!!');
                }
            }
        },
        body:  {
            type: DataTypes.TEXT,
            allowNull: false      }
        // createdAt, updatedAt が自動的に生成される。
        // underscored: true にすると, DB 上は created_at, updated_at になる。
    }, {
        // Other model options go here.
        underscored: true
    });

    Article.associate = function(models) {
        // associations can be defined here
        Article.hasMany(models.ArticlesTag);
        // 多対多
        Article.belongsToMany(models.Tag, {through: 'ArticlesTag'});
    };

    return Article;
};
