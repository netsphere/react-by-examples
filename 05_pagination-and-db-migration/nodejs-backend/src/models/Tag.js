'use strict';

const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    class Tag extends Model {
        static associate(models) {
            // define association here
            Tag.hasMany(models.ArticlesTag);
            // 多対多
            Tag.belongsToMany(models.Article, {through: 'ArticlesTag'});
        }
    };

    Tag.init({
        id: {
            type:          DataTypes.INTEGER,
            primaryKey:    true,
            autoIncrement: true     },
        name: {
            type:      DataTypes.STRING,
            allowNull: false,
            unique:    true,
            validate: { notEmpty: true }  }
    }, {
        sequelize,
        // Other model options go here.
        underscored: true,
        hooks: {
            // beforeCreate + beforeUpdate
            beforeSave: (instance, options) => {
                console.log('Here, beforeSave()!'); // DEBUG
                instance.name = instance.name.normalize('NFKC');
            }
        }
    });

    return Tag;
};
