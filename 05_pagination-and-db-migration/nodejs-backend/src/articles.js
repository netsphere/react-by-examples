'use strict'; // Node.js は明示しないといけない.

// ルーティング (とハンドラ本体) を定義する
// URL '/api/articles'

const express = require('express');
const models = require('./models');

const router = express.Router();

const ResourceUrl = '/:id';

// List
router.get('/', async function(request, response) {
    // For pagination
    //     {"count":1,
    //      "rows":[
    //          {...}
    //      ]
    //     }
    let page = Number(request.query['page']);
    page = page > 0 ? page - 1 : 0; // 1始まり, NaN 対策
    const articles = await models.Article.findAndCountAll({
        limit: 10,
        order: ['id'], // paginate するときは order 必須.
        offset: page * 10 });
    response.json(articles);
});

// Latests
router.get('/latest', async function(request, response) {
    const result = await models.Article.findAll({  // 配列で返す
            order: [['updatedAt', 'DESC']],
            limit: 1 });
    response.json(result);
});


// Show an item
router.get(ResourceUrl, async function(request, response) {
    const result = await models.Article.findOne({
        where: {id:request.params.id},
        include: models.Tag // Tagsプロパティにオブジェクトの配列
    });
    if (!result) {
        // send(body, status) is deprecated.
        return response.status(404).json({
            "error": {
                "code": "RecordNotFound",
                "message": "Record not found",
                "target": "query" }
        });
    }
    response.json(result);
});


// See https://stackoverflow.com/questions/2140627/how-to-do-case-insensitive-string-comparison
function str_ciEquals(a, b) {
    if (a === undefined || a === null || b === undefined || b === null)
        throw new TypeError('a and b must be a String');
    return a.localeCompare(b, undefined, {sensitivity:"accent"}) === 0;
}


//////////////////////////////////////////
// Create new item.

async function update_tags(request_tags, tran) {
    // 重複は削除, 順序は保存する.
    const name_set = [];
    // ''.split(' ') => [""] になる ([]ではない) ことに注意.
    for ( let tag_name of request_tags.trim().split(' ') ) {
        if (tag_name.trim() == '')
            continue;
        let t = tag_name.normalize('NFKC');
        if ( !name_set.some(value => str_ciEquals(value, t)) )
            name_set.push(t);
    }
    console.log(name_set); // DEBUG
    // どうにも findOrCreate() が上手く動かない
    // See https://sequelize.org/master/manual/model-querying-finders.html
    const tags = name_set.map( async tag_name => {
        let tag = await models.Tag.findOne({where: {name:tag_name},
                                            transaction: tran });
            /*const r = await models.Tag.findOrCreate({
                            where: {name:tag_name},
                            transaction: tran  });   */
        if (!tag)
            tag = await models.Tag.create({name:tag_name}, {transaction:tran});

        return tag;
    });
    return tags;
}


// タグとの紐付け. articles_tags レコードが暗黙に作られる.
// 中間テーブルを更新. Create, update 共通.
async function update_articles_tags( article, tags, options ) {
    for ( let tag_promise of tags ) {
        let tag = await tag_promise;
        console.log(tag);
        //await article.setTags(tag /*, {transaction:tran}*/); なぜか動かない
        await models.ArticlesTag.create(
                {articleId:article.id, tagId:tag.id}, options );
    }
}


router.article_create = async function(request, response) {
    var article = null;
    const tr = await models.sequelize.transaction();

    // try-catch ブロックで受け止めるため, sequelize メソッドはすべて await し
    // なければならない。同期なメソッドが Promise を返すのが根本的におかしい.
    try {
        const tags = await update_tags(request.body.tags, tr);

        // create() = build() + save().
        article = await models.Article.create(request.body, {transaction:tr});
        await update_articles_tags(article, tags, {transaction:tr});
    }
    catch (err) {
        // validation error
        await tr.rollback();
        console.log(err); // DEBUG
        return response.status(400).json({
                "error": {
                    "code": "RecordInvalid", // Bad Request
                    "message": err.message || "article_create() failed",
                    "details": err.errors }
        });
    }
    await tr.commit();

    // Tagsプロパティ付きで読み直す  => id だけあればいいので, 省略
    //let result = await models.Article.findOne({
    //                      where:  {id:article.id},
    //                      include:models.Tag });
    response.json(article);
};
router.post('/', router.article_create);


// Update a item.
router.article_update = async function(request, response) {
    var article = await models.Article.findOne({
                            where: {id:request.params.id}
    });
    if (!article) {
        return response.status(404).json({
            "error": {
                "code": "RecordNotFound",
                "message": "Record not found", }
        });
    }

    const tr = await models.sequelize.transaction();
    try {
        await models.ArticlesTag.destroy({ where: {articleId:article.id},
                                            transaction: tr  });
        const tags = await update_tags(request.body.tags, tr);

        article.title = request.body.title;
        article.body = request.body.body;
        await article.save({transaction:tr});
        await update_articles_tags( article, tags, {transaction:tr} );
    }
    catch (err) {
        await tr.rollback();
        return response.status(400).json({
                "error": {
                    "code": "RecordInvalid", // Bad Request
                    "message": err.message || "article_update() failed",
                    "details": err.errors }
        });
    }
    await tr.commit();
    response.json( article );
};
router.put(ResourceUrl, router.article_update);


// destroy
router.delete(ResourceUrl, async function(request, response) {
    const article = await models.Article.findOne({
                            where: {id:request.params.id}
    });
    if (!article) {
        return response.status(404).json({
            "error": {
                "code": "RecordNotFound",
                "message": 'Record not found', }
        });
    }

    const tr = await models.sequelize.transaction();
    try {
        // とりあえず, タグそのものは消さない.
        await models.ArticlesTag.destroy({ where: {articleId:article.id},
                                            transaction: tr });
        await article.destroy({transaction:tr});
    }
    catch (err) {
        await tr.rollback();
        return response.status(400).json({
                "error": {
                    "code": "RecordInvalid", // Bad Request
                    "message": err.message || "delete() failed" }
        });
    }
    await tr.commit();
    response.json(true);
});

module.exports = router;
