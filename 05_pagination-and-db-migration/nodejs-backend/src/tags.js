'use strict'; // Node.js は明示しないといけない.

// ルーティング (とハンドラ本体) を定義する
// URL '/api/tags'

const express = require('express');
const models = require('./models');

const router = express.Router();

const ResourceUrl = '/:id';

// List
router.get('/', async function(request, response) {
    const tags = await models.Tag.findAll({
        include: [{
            model: models.ArticlesTag,
            required: false  // LEFT OUTER JOIN
        }],
        attributes: [
            'id', // これで自動的に tags.id になる
            'name',
            [models.sequelize.fn('count',
                  models.sequelize.col('ArticlesTags.id')), // Bug:名前変換
             'countArticle']
        ],
        group: 'Tag.id' }); // SQL GROUP BY
    response.json(tags);
});


// Show an item
router.get(ResourceUrl, async function(request, response) {
    var result = null;
    if (request.params.name && request.params.name.trim().length > 0) {
        result = await models.sequlize.query(
            'SELECT * FROM tags WHERE UPPER(name) = UPPER(:name)',
            {
                replacements:{name:request.params.name.trim().normalize('NFKC')},
                model: models.Tag
            });
    }
    else {
        result = await models.Tag.findOne({
            where: {id:request.params.id},
            include: models.Article   // Articles プロパティ
        });
    }

    if (!result) {
        // send(body, status) is deprecated.
        response.status(404).json({
            "error": {
                "code": "404",
                "message": "Record not found",
                "target": "query"  }
        });
        return;
    }
    response.json(result);
});


// Update a item.
router.put(ResourceUrl, async function(request, response) {
    let tag = await models.Tag.findOne({
        where: {id:request.params.id}
    });
    if (!tag) {
        response.status(404).json({
            "error": {
                "code": "404",
                "message": "Record not found", }
        });
        return;
    }

    const tr = await models.sequelize.transaction();
    try {
        tag.name = request.body.name.trim().normalize('NFKC');
        // 名前が被ったら、統合する
        // ちょっと複雑 (でもない) なSQLは raw queryで書いた方が早い。ひどい。
        const [existing_tag, metadata] = await models.sequelize.query(
            'SELECT * FROM tags WHERE id <> :id AND UPPER(name) = UPPER(:name)',
            {
                replacements: {id: tag.id, name: tag.name},
                model: models.Tag
                //type: models.sequelize.QueryTypes.SELECT
            });
        if (existing_tag) {
            await models.ArticlesTag.update({tagId:existing_tag.id},
                                             {where:{tagId:tag.id},
                                              transaction: tr});
            await tag.destroy({transaction:tr});
            tag = existing_tag;
        }
        else {
            await tag.save({transaction:tr});
        }
    }
    catch (err) {
        await tr.rollback();
        response.status(400).json({
                "error": {
                    "code": "400", // Bad Request
                    "message": err.message }
        });
        return;
    }

    await tr.commit();
    response.json(tag);
});


// destroy
router.delete(ResourceUrl, async function(request, response) {
    const tag = await models.Tag.findOne({
                          where: {id:request.params.id}
    });
    if (!tag) {
        response.status(404).json({
            "error": {
                "code": 404,
                "message": 'Record not found', }
        });
        return;
    }

    const tr = await models.sequelize.transaction();
    try {
        await models.ArticlesTag.destroy({ where: {tagId:tag.id},
                                            transaction: tr });
        await tag.destroy({transaction:tr});
    }
    catch (err) {
        await tr.rollback();
        response.status(400).json({
                "error": {
                    "code": "400", // Bad Request
                    "message": err.message }
        });
        return;
    }
    await tr.commit();
    response.json(true);
});

module.exports = router;
