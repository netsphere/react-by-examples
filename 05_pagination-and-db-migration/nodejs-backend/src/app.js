'use strict'; // Node.js は明示しないといけない.

// From https://expressjs.com/ja/starter/hello-world.html

const express = require('express');
// ExpressJS v4.16.0 で, body-parser相当の機能が組み込まれた。
//const bodyparser = require('body-parser');

const articles = require('./articles');
const tags = require('./tags');

// Express application
const app = express();

// middleware
//app.use(bodyparser.bodyParser.json());
app.use(express.json());

// Routings
// app.use([path,] callback [, callback...])
app.use('/api/articles', articles);
app.use('/api/tags', tags);

/*
/////////////////////////////////////////////////////
// DEBUG
async function mytest() {
    let request = {
        body: {
            title:"NG",  // validation error
            body: "body\nbody2\nline3",
            tags: "aa bb cc"
        }
    };
    let response = {
        status_code: null,
        res_body: null,
        json: (str) => { this.res_body = JSON.stringify(str); },
        status: (code) => {
            this.status_code = code;
            return this; }
    };
    await articles.articles_create(request, response);
    console.log(response);
}
mytest();
console.log("///////////////////////////////");
// DEBUG
/////////////////////////////////////////////////////
*/

// 起動
const PORT = process.env.PORT || 3001;

// backend は, localhost に限定しなければならない
app.listen(PORT, 'localhost', function() {
    console.log("Example app listening on port %d in '%s' mode",
                PORT, app.settings.env);
});
