
// rc-pagination は class 指定が決め打ち。使えない。
import ReactPaginate from 'react-paginate';

// スタイルを適用する
export default function Pagination({total, pageSize, ...props}) {
    return (
    <ReactPaginate
        pageCount={Math.floor(total / pageSize) + 1} // 除算は実数
        containerClassName='pagination'
        pageClassName='page-item'        // li
        pageLinkClassName='page-link'    // a
        activeClassName='active'
        previousClassName='page-item'    // li
        nextClassName='page-item'        // li
        previousLinkClassName='page-link'  // a
        nextLinkClassName='page-link'      // a
        disabledClassName='disabled'
        {...props}    // 単に props と書くだけではダメ。展開してやる。
    /> );
}
