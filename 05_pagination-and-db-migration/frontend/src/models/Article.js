
import API from './api';

class Article extends API
{
    // @override
    static async create(item) {
        const id = Number(item.id);

        let article = {
            title: item.title.trim(),
            body: item.body,
            tags: item.tags.join(' ') // 空白区切りの文字列に変換
        };
        if (id > 0)
            article['id'] = id; // update

        return super.create(article);
    }

    static async find_latest() {
        const model_name = this.name.toLowerCase() + "s";
        const response = await API.apicall('GET',
                                           "/" + model_name + `/latest`);
        const res_body = await response.json();
        if (res_body.error) {
            throw new ApiError(response.status,
                               res_body.error.message || "find_latest() error",
                               res_body);
        }
        return res_body;
    }
} // class Article

export default Article;
