
import React, {useState, useEffect} from 'react';
// <Redirect> は react-router v6 で単に廃止。<Navigate> に置き換え.
import { Link, useParams, useNavigate } from 'react-router-dom';
import {useForm} from 'react-hook-form';
import Tag from '../../models/Tag';


const validator = async (values, context) => {
    let errors = {};
    const name = values.name.normalize('NFKC').trim();
    if (!name) {
        errors['name'] = {
            type: 'required',
            message: 'Name field must have at least 1 character.' };
    }

    return {
        values: Object.keys(errors).length > 0 ? {} : values,
        errors: errors };
};


// 編集ページ
function TagEdit() // extends React.Component
{
    // react-router v6: URL 引数の受け取り方が変わった。ヒドい。
    const params = useParams();
    // react-router v6: リダイレクトのやり方も変わった。
    const navigate = useNavigate();
    // ここで validator を指定する.
    const { register, handleSubmit, setError, reset, formState:{errors} } =
          useForm({mode:'onChange',
                   resolver:validator,
                   defaultValues: {
                       id: null,
                       name: ''}   });

    const [state, setState] = useState({
        isLoaded:false,
        //error:null,
        //redirectToShow: false,
        submitEnabled: true, //false,
    });

    let { id } = params;
    useEffect( () => { // componentDidMount() {
        if ( !state.isLoaded ) {
            if (Number(id) > 0) {
                // edit
                Tag.find(id).then( post => {
                    setState({...state,
                              isLoaded: true});
                    reset(post);
                });
            }
            else {
                // new
                throw new RangeError('internal error');
            }
        }
    });

    const onSubmit = (formData) => {
        const post = {
            id:   formData.id,
            name: formData.name.normalize('NFKC').trim()
        };
        Tag.create(post)
            .then( tag => {
                navigate(`/tags/${tag.id}`,
                         {state:{flash:`Item "${tag.name}" updated.`}});
            }).catch( err => {
                setError( 'root', err.response.error );
            });
    }
/*
    onFieldChanged = (event) => {
        const name = event.target.name;
        // checkbox の場合は, 場合分け必要.
        const value = event.target.value;

        // TODO: クライアントでも, tag の簡易な重複チェック.
        this.setState( {[name]:value} );
        if (name === 'name')
            this.setState( {submitEnabled: value !== ''} );
    } */

    return (<>
  {errors['root'] ? <div class="alert alert-danger" role="alert">
                    Error: {errors['root'].message}</div> : ''}
  { !state.isLoaded ? (
  <div>Loading...</div>
  ) : (
  <div>
  {Number(id)}
    <form onSubmit={handleSubmit(onSubmit)} >
      <div class="form-group row">
        <label className="col-sm-2 col-form-label">Name:</label>
        <div class="col-sm-10">
          <input name="name" type="text" class="form-control" {...register("name")} />
            {errors.name ? <div >{errors.name.message}</div> : ''}
        </div>
      </div>
      <Link to={`/tags/${id}`} className="btn btn-secondary">Cancel</Link>
      <button type="submit" class="btn btn-primary" disabled={state.submitEnabled ? '' : 'disabled'}>
        {Number(id) > 0 ? 'Update' : 'Create'}</button>
    </form>
  </div> )}
            </>);
}


export default TagEdit;
