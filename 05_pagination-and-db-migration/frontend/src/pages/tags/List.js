
import React, {useState, useEffect} from 'react';
// react-router v6: location の取り出し方が変わった
// React Hook "useLocation" cannot be called in a class component. React Hooks
// must be called in a React function component or a custom React Hook function.
import { Link, useLocation } from 'react-router-dom';
import Tag from '../../models/Tag';


function TagList() // extends React.Component
{
    // react-router v6: props.location が来てない。ヒドい。
    const location = useLocation();

    // Ajax 呼び出ししてよいタイミングは限定されている.
    // See https://reactjs.org/docs/faq-ajax.html
    const [state, setState] = useState({
        flash: location.state ? location.state.flash : null,
        error:    null,
        isLoaded: false,
        tags: []   });

    useEffect( () => { // componentDidMount() {
        if ( !state.isLoaded ) {
            Tag.find_all().then( tags => {
                setState({...state,
                          isLoaded: true,
                          error: null,
                          tags: tags      });
            }).catch( err => {
                setState( {...state, error:err });
            });
        }
    });

    return (<>
  <h1>タグ一覧</h1>
  { state.error ? <div class="alert alert-danger" role="alert">
                    Error: {state.error.constructor.name} {state.error.message}
                  </div> : ''}
  { !state.isLoaded ? (
  <div>Loading...</div>
  ) : (
  <div>
  { state.flash ? <div class="alert alert-success">{state.flash}</div> : ''}
    <table class="table">
      <thead>
        <tr><th>#</th> <th>Name</th></tr>
      </thead>
      <tbody>
          { state.tags.map(tag => (
        <tr>
          <td>{tag.id}</td> <td>{tag.name}</td> <td>{tag.countArticle}</td>
          <td><Link to={`/tags/${tag.id}`}>Show</Link></td>
        </tr>
            ))}
      </tbody>
    </table>
  </div> )}
            </>);
}


export default TagList;
