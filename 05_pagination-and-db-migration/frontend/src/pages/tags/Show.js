
import React, { useState, useEffect } from 'react';
// - <Redirect> は react-router v6 で単に廃止。<Navigate> に置き換え.
// - React Hook "useParams" cannot be called in a class component. React Hooks
//   must be called in a React function component or a custom React Hook
//   function.
import { Link, useParams, useNavigate, useLocation } from 'react-router-dom';
import Tag from '../../models/Tag';


function TagShow() // extends React.Component
{
    // react-router v6: URL 引数の受け取り方が変わった。ヒドい。
    const params = useParams();
    // react-router v6: リダイレクトのやり方も変わった。
    const navigate = useNavigate();
    const location = useLocation();

    // Ajax 呼び出ししてよいタイミングは限定されている.
    // See https://reactjs.org/docs/faq-ajax.html
    let init = {
        flash: location.state ? location.state.flash : null,
        error:    null,
        //isLoaded: false,
        //redirectToList: false,
        tag: null  };

    // URLが不正?
    let { id } = params;
    if ( !(Number(id) > 0) ) {
        init = {...init, tag:null,
                error: {
                    'code': 400,
                    'message': 'parameter "id" is invalid' }};
    }

    const [state, setState] = useState(init);

    useEffect( () => { // componentDidMount() {
        if ( state.error )
            return;
        if ( !state.tag ) {
            let { id } = params;
            Tag.find(id).then( tag=> {
                setState({...state,
                          //isLoaded: true,
                          error: null,
                          tag: tag      });
            }).catch( err => {
                setState( {...state, error:err, tag:null });
            });
        }
    });

    const onDelete = () => {
        //const tag = this.state.tag;
        if (state.tag &&
            window.confirm(`Are you sure you want to delete "${state.tag.name}"?`)) {
            Tag.delete(state.tag.id).then( result => {
                navigate(`/tags/`,
                         {state: {flash:`Item "${state.tag.name}" removed.`}});
            });
        }
    };

    return (<>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">Home</a></li>
    <li class="breadcrumb-item" ><a href="/tags/">Tags</a></li>
  </ol>
</nav>
  {state.error ? <div class="alert alert-danger" role="alert">Error: {state.error.constructor.name} {state.error.message}</div> : ''}
  { !state.tag ? (
  <div>Loading...</div>
  ) : (
  <div>
    <h1>タグ {state.tag.name}</h1>
    {state.flash ? <div>{state.flash}</div> : ''}
    <Link to={`/tags/${state.tag.id}/Edit`}>編集...</Link>
    <button type="button" onClick={onDelete}>Delete</button>
    {state.tag.Articles.map(article => (
              <div>{article.id} {article.title}
                <Link to={`/articles/${article.id}`}>Show</Link>
              </div>
            ))}
  </div> )}
            </>);
}


export default TagShow;
