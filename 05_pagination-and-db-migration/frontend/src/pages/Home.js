
import React from 'react';
import { Link } from 'react-router-dom';
import Article from '../models/Article'


class Home extends React.Component
{
    // articles/Show.js に似たように作る
    constructor(props) {
        super(props);
        this.state = {
            isLoaded: false,
            error: null,
            post: null };
    }

    componentDidMount() {
        if ( this.state.error )
            return;
        
        Article.find_latest().then( posts => {
            this.setState({
                isLoaded: true,
                error: null,
                post: posts[0]  });
        })
        .catch( err => {
            this.setState( {error: err, isLoaded: true} );
        });
    }

    
    render() {
        const { error, isLoaded, post } = this.state;
        if ( error ) {
            return <div class="alert alert-danger" role="alert">Error: {error.message}</div>;
        }
        if (!isLoaded) {
            return <div>Loading...</div>;
        }

        return <div>
  <h1>Home</h1>
                   { post ? <div>{post.title}
                              <Link to={`/articles/${post.id}`}>Show</Link>  
                            </div> : '' }     
  <Link to={`/articles/`}>List</Link>
</div> ;
    }
}

export default Home;
