
import React, {useState, useEffect} from 'react';
// <Redirect> は react-router v6 で単に廃止。<Navigate> に置き換え.
import { Link, useParams, useNavigate } from 'react-router-dom';
import {useForm} from 'react-hook-form';
import Article from '../../models/Article';


// Validation は、それぞれのフィールドに付けるのが簡単。
// フィールドを跨ぎたいなどの場合は、Resolver<> を作る
// @param context useForm() に渡した値.
const validator = async (values, context) => {
    let errors = {};
    const title = values.title.trim();
    if (!title) {
        errors['title'] = {
            type: 'required',
            message: 'Title field must have at least 1 character.' };
    }

    return {
        values: Object.keys(errors).length > 0 ? {} : values,
        errors: errors };
};


// 編集ページ
function ArticleEdit() // extends React.Component
{
    // react-router v6: URL 引数の受け取り方が変わった。ヒドい。
    const params = useParams();
    // react-router v6: リダイレクトのやり方も変わった。
    const navigate = useNavigate();
    const { register, handleSubmit, setError, reset, formState:{errors} } =
          useForm({mode:'onChange',
                   resolver:validator,
                   defaultValues: {
                       id: null,
                       title: '',
                       tags: '',
                       body: '' }  });

    let { id } = params;
    const [state, setState] = useState({
        isLoaded: Number(id) > 0 ? false : true,
        //error: null,
        submitEnabled: true,//false,
    });

    useEffect( () => { // componentDidMount() {
        //let { id } = params;
        if (Number(id) > 0 && !state.isLoaded ) {
            // edit
            Article.find(id).then( post => {
                console.log('useEffect() post = ', post);
                setState({...state,
                          isLoaded: true});
                // Field data は state とは別になる。
                reset({...post,
                    //id:    post.id,
                    //title: post.title,
                    //body:  post.body,
                       tags:  post.Tags.map(x => x.name).join(' ') });
            });
        }
        /* else {
            // new
            this.setState({
                isLoaded: true,
                submitEnabled: false,   });
        } */
    });

    const onSubmit = (formData) => {
        //event.preventDefault();
        const post = {
            id:    formData.id,
            title: formData.title.trim(),
            body:  formData.body,
            tags:  formData.tags.normalize('NFKC').trim().split(' ')  // 空白が複数のとき, '' の項目ができる。-> サーバ側で削除する。
        };
        Article.create(post)
            .then( article => {
                // Succeeded. Redirect
                navigate(`/articles/${article.id}`,
                         {state:{flash:`Item "${article.title}" updated.`}});
            }).catch( err => {
                setError('root', err.response.error);
            });
    }

    return (<>
  {errors['root'] ? <div class="alert alert-danger" role="alert">
                    Error: {errors['root'].message}</div> : ''}
  { !state.isLoaded ? (
  <div>Loading...</div>
                ) : (
  <div>
    <form onSubmit={handleSubmit(onSubmit)} >
      <div class="row mb-3">
        <label className="col-sm-2 col-form-label">Title:</label>
        <div class="col-sm-10">
          <input name="title" type="text" class="form-control"
                 {...register("title")} />
            {errors.title ?
             <div >{errors.title.message}</div> : ''}
        </div>
      </div>
      <div class="row mb-3">
        <label className="col-sm-2 col-form-label">Tags:</label>
        <div class="col-sm-10">
          <input name="tags" type="text" class="form-control"
                   {...register("tags") } /> 空白区切り
        </div>
      </div>
      <div class="row mb-3">
        <label className="col-sm-2 col-form-label">Body:</label>
        <div class="col-sm-10">
          <textarea name="body" class="form-control"
                      {...register("body") } />
        </div>
      </div>
      <Link to={id ? `/articles/${id}` : `/articles/`}
            replace className="btn btn-secondary">Cancel</Link>
      <button type="submit" class="btn btn-primary"
              disabled={state.submitEnabled ? '' : 'disabled'}>
        {Number(id) > 0 ? 'Update' : 'Create'}</button>
    </form>
  </div> )}
            </> );
}


export default ArticleEdit;
