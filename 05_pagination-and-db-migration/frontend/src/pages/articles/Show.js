
import React, { useState, useEffect } from 'react';
// - <Redirect> は react-router v6 で単に廃止。<Navigate> に置き換え.
// - React Hook "useParams" cannot be called in a class component. React Hooks
//   must be called in a React function component or a custom React Hook
//   function.
import { Link, useParams, useNavigate, useLocation } from 'react-router-dom';
import Article from '../../models/Article'


// 改行を <br /> に変換
function nl2br(text) {
    // gフラグ: Global search.
    const re = /(\r?\n)/g
    return text.split(re).map(function(line) {
            if (line.match(re))
                return React.createElement('br');
            else
                return line;
    });
}


function ArticleShow() // extends React.Component
{
    // react-router v6: URL 引数の受け取り方が変わった。ヒドい。
    const params = useParams();
    // react-router v6: リダイレクトのやり方も変わった。
    const navigate = useNavigate();
    const location = useLocation();

    let init = {
        flash: location.state ? location.state.flash : null,
        //isLoaded: false,
        error: null,
        //redirectToList: false,
        post: null  };

    // URLが不正?
    let { id } = params;
    if ( !(Number(id) > 0) ) {
        init = {...init, post:null,
                error: {
                    'code': 400,
                    'message': 'parameter "id" is invalid' }};
    }

    const [state, setState] = useState(init);

    useEffect( () => { // componentDidMount() {
        if ( state.error )
            return;
        if ( !state.post ) {
            let { id } = params;
            Article.find(id).then( post => {
                setState({...state,
                          //isLoaded: true,
                          error: null,
                          post: post     });
            }).catch( err => {
                setState( {...state, error: err, post:null });
            });
        }
    });

    const onDelete = () => {
        //const post = this.state.post;
        if (state.post &&
            window.confirm(`Are you sure you want to delete "${state.post.title}"?`)) {
            Article.delete(state.post.id).then( result => {
                navigate(`/articles/`,
                         {state: {flash:`Item "${state.post.title}" removed.`}});
            });
        }
    };

    return (<>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/">Home</a></li>
    <li class="breadcrumb-item" ><a href="/articles/">Articles</a></li>
  </ol>
</nav>
  {state.error ? <div class="alert alert-danger" role="alert">
                   Error: {state.error.message}</div> : ''}
  { !state.post ? (
  <div>Loading...</div>
  ) : (
  <article>
    <h1>{state.post.title}</h1>
    {state.flash ? <div class="alert alert-success">{state.flash}</div> : ''}
  Tag(s): {state.post.Tags.map(tag => (
            <span><Link class="uk-button uk-button-small uk-button-secondary" to={`/tags/${tag.id}`}>{tag.name}</Link> &nbsp; </span>)
                                  )}
    <div>
              { nl2br(state.post.body) }
    </div>
    <Link to={`/articles/${state.post.id}/edit`} className="btn btn-secondary">Edit...</Link>
    <button type="button" onClick={onDelete} className="btn btn-secondary">Delete</button>
  </article> )}
            </>);
}


export default ArticleShow;
