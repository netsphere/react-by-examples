
import React from 'react'
import TodoListRow from './TodoListRow'
import Footer from './Footer';


class TodoList extends React.Component
{
    // props: todos, onFinish, selectedFilter, onChangeFilter
    constructor(props) {
        super(props);
        this.state = {}
    }

    // コンポーネントから別のコンポーネントを呼び出せる.
    render() {
        return (<div>
  <Footer selectedFilter={this.props.selectedFilter}
                onChangeFilter={this.props.onChangeFilter} />
  {this.props.todos.map(todo => (
        <TodoListRow key={todo.id} {...todo}
                     onClick={() => this.props.onFinish(todo.id)} />
      ))}
</div> );
    }
}

export default TodoList;
