import React from 'react'
import PropTypes from 'prop-types'

const LinkButton = ({ active, children, onClick }) => (
  <button onClick={onClick}
          disabled={active}
          style={{
              marginLeft: '4px'
          }} >
    {children}
  </button>
)

// コンストラクタに渡される props の型チェック
LinkButton.propTypes = {
  active: PropTypes.bool.isRequired,
  children: PropTypes.node.isRequired,
  onClick: PropTypes.func.isRequired
}

export default LinkButton
