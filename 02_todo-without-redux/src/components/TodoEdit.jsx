// フォーム

import React from 'react'

// See https://ja.reactjs.org/docs/forms.html
class TodoEdit extends React.Component
{
    // props: onAdd
    constructor(props) {
        // props は this.props としてアクセス可能.
        super(props);
        this.state = {
            submitEnabled: false,
            todoText: ''  // 現に表示している内容
        };

        // 現代は, bind() よりも、アロー関数を使う.
        //this.handleSubmit = this.handleSubmit.bind(this);
    }

    // submit() 内で this.state で値を取れるようにする.
    handleTodoTextChanged = (event) => {
        const value = event.target.value;
        this.setState( {
            todoText:value,
            submitEnabled: value.trim() !== ''  } );
    }
    
    handleSubmit = (event) => {
        // サーバに送信しない。後の例で, 送信するようにする.
        event.preventDefault(); 
        const text = this.state.todoText.trim();
        if (!text)
            return;
        this.props.onAdd(text);
        this.setState( {todoText: ''} );
    }

    render() {
        return (<div>
  <form onSubmit={this.handleSubmit} >
    <input value={this.state.todoText} onChange={this.handleTodoTextChanged} />
    <button type="submit" disabled={this.state.submitEnabled ? '' : 'disabled'}>
      Add Todo</button>
  </form>
</div> );
    }
}

export default TodoEdit
