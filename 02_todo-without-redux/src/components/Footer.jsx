
import React from 'react'
import FilterButton from './FilterButton'
import { VisibilityFilters } from '../TodoModel'

// VisibilityFilters はハッシュで、値が文字列なのを利用して、ボタンテキストを作
// る.
const buttonText = filter => {
    let text = filter.replace(/^SHOW_/, '');
    return `${text[0]}${text.substring(1).toLowerCase()}`;
}

// React のコンポーネントは, React.Component から派生する代わりに、関数一つでも
// よい.
const Footer = ( {selectedFilter, onChangeFilter} ) => (<div>
  <span>Show: </span>
    {[VisibilityFilters.SHOW_ALL,
      VisibilityFilters.SHOW_ACTIVE,
      VisibilityFilters.SHOW_COMPLETED
     ].map(filter => 
      <FilterButton key={filter} active={selectedFilter === filter}
                  onClick={() => {onChangeFilter(filter)}} >
        {buttonText(filter)}
      </FilterButton>
    )}
</div>
)

export default Footer

