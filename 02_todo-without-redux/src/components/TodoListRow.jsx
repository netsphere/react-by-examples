import React from 'react'
import PropTypes from 'prop-types'

// コンポーネント名は大文字で始めなければならない。
// JSX の属性値は, JSON プロパティに展開されるため, 式でなければならない。特定
// の場合だけ要素を表示する場合は, 下のように条件式か、あるいは3項演算子を使う.
const TodoListRow = ({ onClick, completed, text }) => (<div>
  <span style={{
          textDecoration: completed ? 'line-through' : 'none' }} >
      {text}
  </span>
    {!completed && <button type="button" onClick={onClick}>Done</button>}      
</div> );

TodoListRow.propTypes = {
  onClick: PropTypes.func.isRequired,
  completed: PropTypes.bool.isRequired,
  text: PropTypes.string.isRequired
}

export default TodoListRow
