
export const VisibilityFilters = {
    SHOW_ALL: 'SHOW_ALL',
    SHOW_COMPLETED: 'SHOW_COMPLETED',
    SHOW_ACTIVE: 'SHOW_ACTIVE'
}

// この例ではデータベースにはアクセスしない。
export class TodoModel
{
    // テストデータ
    static todos = [
        {id: 1, text: 'test todo', completed: false},
        {id: 2, text: 'fini<big>shed', completed: true},
    ]

    static nextId = 3;

    static create(newItem) {
        const todoItem = {
            id:        TodoModel.nextId,
            text:      newItem.text.trim(),
            completed: newItem.completed };
        TodoModel.todos = [...TodoModel.todos, todoItem]
        ++TodoModel.nextId;
    }
    
    static find_all(filter) {
        switch (filter)
        {
        case VisibilityFilters.SHOW_ALL:
            return TodoModel.todos
        case VisibilityFilters.SHOW_COMPLETED:
            return TodoModel.todos.filter(t => t.completed)
        case VisibilityFilters.SHOW_ACTIVE:
            return TodoModel.todos.filter(t => !t.completed)
        default:
            throw new Error('Unknown filter: ' + filter)
        }
    }

    static finish_todo(id) {
        TodoModel.todos = TodoModel.todos.map(
            todo => todo.id === id ? {...todo, completed:true} : todo);
    }
}

