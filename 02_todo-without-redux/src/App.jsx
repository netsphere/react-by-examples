
// `App` component

import React from 'react';
import TodoEdit from './components/TodoEdit';
import TodoList from './components/TodoList';
import {TodoModel, VisibilityFilters} from './TodoModel';

class App extends React.Component
{
    constructor(props) {
        super(props);
        // コンポーネントを跨ぐ状態についてはその外側のコンポーネントに保存.
        this.state = {
            // view model
            todos: TodoModel.find_all(VisibilityFilters.SHOW_ALL),
            // config
            filter: VisibilityFilters.SHOW_ALL,
        };
    }

    // Redux の例では, actions/index.js 内で定義
    // state が App クラスにあるので、ここで定義する.
    onAdd = (text) => {
        TodoModel.create({
            text: text,
            completed: false
        });
        this.setState( {
            todos: TodoModel.find_all(this.state.filter),
        });
    }

    onFinish = (id) => {
        TodoModel.finish_todo(id);
        this.setState( {
            todos: TodoModel.find_all(this.state.filter) });
    }

    onChangeFilter = (filter) => {
        this.setState( {
            todos: TodoModel.find_all(filter),
            filter: filter} );
    }

    render() {
        return (<div className="App">
  <h1>Todo List</h1>

  <TodoList todos={this.state.todos} onFinish={this.onFinish}
                selectedFilter={this.state.filter}
                onChangeFilter={this.onChangeFilter} />

  <TodoEdit onAdd={this.onAdd} />
</div> );
    }
}

export default App;
