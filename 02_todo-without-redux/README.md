
# 複数のコンポーネントを用いる Reactサンプル

<i>Vite</i>, React 19.

No Redux version. データの永続化は行わない。

From https://redux.js.org/basics/example
この例を、Redux を使わないように修正。


## How to run

```
  $ npm install
  $ npm start
```


